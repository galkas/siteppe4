<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AfficheAnnoncesType5 extends Model
{
    protected $table = 'annoncestype5s';
}
