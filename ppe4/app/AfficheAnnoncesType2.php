<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AfficheAnnoncesType2 extends Model
{
    protected $table = 'annoncestype2s';
}
