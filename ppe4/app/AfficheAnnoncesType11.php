<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AfficheAnnoncesType11 extends Model
{
    protected $table = 'annoncestype11s';
}
