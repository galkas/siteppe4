<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AfficheAnnoncesType7 extends Model
{
    protected $table = 'annoncestype7s';
}
