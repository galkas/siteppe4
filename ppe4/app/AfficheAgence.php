<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AfficheAgence extends Model
{
    protected $table = 'agences';
}
