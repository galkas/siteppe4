<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AfficheAnnoncesType21 extends Model
{
    protected $table = 'annoncestype21s';
}
