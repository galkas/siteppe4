<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AfficheAnnoncesType6 extends Model
{
    protected $table = 'annoncestype6s';
}
