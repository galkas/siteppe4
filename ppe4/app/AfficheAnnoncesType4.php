<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AfficheAnnoncesType4 extends Model
{
    protected $table = 'annoncestype4s';
}
