<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AfficheAnnoncesType12 extends Model
{
    protected $table = 'annoncestype12s';
}
