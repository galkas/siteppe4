<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AfficheAnnoncesType13 extends Model
{
    protected $table = 'annoncestype13s';
}
