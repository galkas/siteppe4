<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AfficheAnnoncesType14 extends Model
{
    protected $table = 'annoncestype14s';
}
