<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AfficheAnnoncesType1 extends Model
{
    protected $table = 'annoncestype1s';
}
