<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AfficheAnnoncesType3 extends Model
{
    protected $table = 'annoncestype3s';
}
