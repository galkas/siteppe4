<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnnoncesType7 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annoncestype7s',function($table) {
		$table->integer('NO_ASP');
		$table->primary('NO_ASP');
		$table->foreign('NO_ASP')
		      ->references('NO_ASP')
		      ->on('annonces');
		$table->timestamps();
		$table->integer('id');
		$table->string('CODE_SOCIETE',4);
		$table->string('CODE_SITE',2);
		$table->string('NO_DOSSIER',5);
		$table->string('NO_RUE',10);
		$table->string('TYPE_RUE',15);
		$table->string('NO_MANDAT',10);
		$table->string('TYPE_MANDAT',15);
		$table->string('ADR',32);
		$table->string('SUITE_ADR',32);
		$table->string('CP',5);
		$table->string('VILLE',26);
		$table->string('PAYS',25);
		$table->string('CP_WEB',5);
		$table->string('VILLE_WEB',26);
		$table->string('CAT',25);
		$table->string('URL_VIDEO',150);
		$table->integer('TYPE_BIEN');
		$table->integer('ETAT_AVANC');
		$table->boolean('CC');
		$table->date('DATE_CREATION');
		$table->date('DATE_MODIF');
		$table->date('DATE_MAND');
		$table->date('DATE_FIN_MAND');
		$table->string('TXT_INTERNET',1024);
		$table->string('SECTEUR',100);
		$table->string('CRIT_COMP',512);
		$table->date('DATE_INIT_PH');
		$table->string('NOM_CONTACT',25);
		$table->string('TEL_CONTACT',20);
		$table->string('DIST',20);
		$table->float('LAT');
		$table->float('LON');
		$table->boolean('PRIX_CONF');
		$table->text('MOTS_CLES');
		$table->integer('CONTACT');
		$table->foreign('CONTACT')
		      ->references('NO_ASP')
		      ->on('contacts');
		$table->text('LANGUES');
		$table->foreign('LANGUES')
		      ->references('LANGUE')
		      ->on('langues');
		$table->integer('AGENCE');
		$table->foreign('AGENCE')
		      ->references('NO_ASP')
		      ->on('agences');
		
		//$table->integer('PIECE');
		//$table->foreign('PIECE')
		//      ->references('NO_ASP')
		//      ->on('pieces');
		//$table->text('ATTRIBUTS');
		//$table->foreign('ATTRIBUTS')
                //      ->references('ATTRIBUT')
                //      ->on('attributs');
		//$table->integer('PHOTOS');
		//$table->foreign('PHOTOS')
		//      ->references('NO_ASP')
		//      ->on('photos');

		$table->string('QUARTIER',25);
		$table->string('NOM_PARK',25);
		$table->string('TRANSPORT',25);
		$table->string('PROXIMITE',25);
		$table->string('SITUATION',25);
		$table->string('REG_FISC',25);
		$table->string('FORM_JUR',25);
		$table->string('TYPE_CONS',25);
		$table->string('ETAT_GEN',25);
		$table->string('DIGICODE',10);
		$table->float('PV');
		$table->float('HONO');
		$table->float('TX_HONO');
		$table->float('TAXE_FONC');
		$table->float('TAXE_PROF');
		$table->float('LOYER');
		$table->float('SURF');
		$table->integer('NIVEAU');
		$table->integer('NB_NIV');
		$table->integer('TYPE_STAT');
		$table->integer('TYPE_ACCES');
		$table->integer('ACCES_PIETON');
		$table->integer('NB_VOITURE');
		$table->integer('ANNEE_CONS');
		$table->boolean('ASCE');
		$table->boolean('HAND');
		$table->boolean('BIP');
		$table->boolean('CARTE_MAGN');
		$table->boolean('VIDEO_SURV');
		$table->date('DATE_LIBER');
		$table->date('DATE_MODIF_PRIX');
		$table->string('TXT_PRESSE',1024);
		$table->float('LONGUEUR');
		$table->float('LARGEUR');
		$table->float('HAUTEUR');
		$table->boolean('GARDIENNAGE');
		$table->string('NUM_EMP',10);
		$table->string('NO_PRIV',10);
		$table->integer('CH_HONO');
		$table->boolean('CPG_LAF');
		$table->integer('COPROPRIETE');
		$table->integer('NB_LOTS_COPRO');
		$table->float('MONTANT_QUOTEPART');
		$table->integer('PROCEDURE_SYND');
		$table->string('PROCEDURE_DETAIL',128);
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
