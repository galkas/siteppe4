<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Surfaces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surfaces',function($table) {
		$table->integer('NO_ASP');
		$table->primary('NO_ASP');
		$table->integer('NO_ASP_ANNONCE');
		$table->foreign('NO_ASP_ANNONCE')
		      ->references('NO_ASP')
		      ->on('annonces');
		$table->timestamps();
		$table->integer('id');		
		$table->string('NAT',40);
		$table->string('VUE',15);
		$table->string('COMMENTAIRES',30);
		$table->string('NO_ORDRE',2);
		$table->float('SURFACE');
		$table->float('HSP');
		$table->integer('ETAGE');
		$table->float('MOYER_M2');
		$table->float('SURF_MINI');
		$table->float('CHARGES_M2');
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
