<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Attributs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributs',function($table) {
		$table->string('ATTRIBUT');
		$table->primary('ATTRIBUT');
		$table->integer('NO_ASP_ANNONCE');
		$table->foreign('NO_ASP_ANNONCE')
		      ->references('NO_ASP')
		      ->on('annonces');
		$table->timestamps();
		$table->integer('id');
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
