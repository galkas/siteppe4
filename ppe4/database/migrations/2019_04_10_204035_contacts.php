<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Contacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts',function($table) {
		$table->integer('NO_ASP');
		$table->primary('NO_ASP');
		$table->integer('id');
		$table->timestamps();
		$table->string('CIVILITE',15);
		$table->string('NOM',32);
		$table->string('PRENOM',20);
		$table->string('ADRESSE',32);
		$table->string('SUITE_ADRESSE',32);
		$table->string('CP',5);
		$table->string('VILLE',26);
		$table->string('PAYS',25);
		$table->string('TEL',20);
		$table->string('FAX',20);
		$table->string('MOBILE',20);
		$table->string('MAIL',50);
		$table->string('LIBELLE',25);
		$table->string('FONCTION',25);
		$table->string('URL_PHOTO',25);
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
