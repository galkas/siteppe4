<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LotsNeufs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lotsneufs',function($table) {
		$table->integer('NO_ASP');
		$table->primary('NO_ASP');
		$table->foreign('NO_ASP')
		      ->references('NO_ASP')
		      ->on('annonces');
		$table->timestamps();
		$table->integer('id');
		$table->integer('ETAT_AVANC');
		$table->integer('TYPE_BIEN');
		$table->string('CAT',25);
		$table->string('NO_LOT',10);
		$table->integer('NB_PCE');
		$table->integer('NB_CHB');
		$table->float('SURF');
		$table->integer('ETAGE');
		$table->integer('NB_ETAGE');
		$table->float('SURF_JAR');
		$table->float('SURF_TERR');
		$table->string('VUE',25);
		$table->string('EXPO',2);
		$table->float('PV');
		$table->date('DATE_LIV');
		$table->integer('NB_PARK');
		$table->string('TYPE_PARK',25);
		$table->float('PRIX_PARK');
		$table->float('LOYER_GAR');
		$table->float('RENTAB');
		$table->string('NO_PLACE',15);
		$table->string('TXT_INTERNET',1024);
		$table->string('BAT',25);
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
