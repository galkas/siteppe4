<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pieces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pieces',function($table) {
		$table->integer('NO_ASP');
		$table->primary('NO_ASP');
		$table->integer('NO_ASP_ANNONCE');
		$table->foreign('NO_ASP_ANNONCE')
		      ->references('NO_ASP')
		      ->on('annonces');
		$table->timestamps();
		$table->integer('id');
		$table->string('DESIGNATION',40);
		$table->string('NAT_SOL',15);
		$table->string('VUE',15);
		$table->string('EXPO',2);
		$table->string('COMMENTAIRES',30);
		$table->string('NO_ORDRE',2);
		$table->float('SURFACE');
		$table->float('HSP');
		$table->integer('NIVEAU');
		$table->integer('NO_ASP_PHOTO');
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
