<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnnoncesType11 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annoncestype11s',function($table) {
		$table->integer('NO_ASP');
		$table->primary('NO_ASP');
		$table->foreign('NO_ASP')
		      ->references('NO_ASP')
		      ->on('annonces');
		$table->timestamps();
		$table->integer('id');
		$table->string('CODE_SOCIETE',4);
		$table->string('CODE_SITE',2);
		$table->string('NO_DOSSIER',5);
		$table->string('NO_RUE',10);
		$table->string('TYPE_RUE',15);
		$table->string('NO_MANDAT',10);
		$table->string('TYPE_MANDAT',15);
		$table->string('ADR',32);
		$table->string('SUITE_ADR',32);
		$table->string('CP',5);
		$table->string('VILLE',26);
		$table->string('PAYS',25);
		$table->string('CP_WEB',5);
		$table->string('VILLE_WEB',26);
		$table->string('CAT',25);
		$table->string('URL_VIDEO',150);
		$table->integer('TYPE_BIEN');
		$table->integer('ETAT_AVANC');
		$table->boolean('CC');
		$table->date('DATE_CREATION');
		$table->date('DATE_MODIF');
		$table->date('DATE_MAND');
		$table->date('DATE_FIN_MAND');
		$table->string('TXT_INTERNET',1024);
		$table->string('SECTEUR',100);
		$table->string('CRIT_COMP',512);
		$table->date('DATE_INIT_PH');
		$table->string('NOM_CONTACT',25);
		$table->string('TEL_CONTACT',20);
		$table->string('DIST',20);
		$table->float('LAT');
		$table->float('LON');
		$table->boolean('PRIX_CONF');
		$table->text('MOTS_CLES');
		$table->integer('CONTACT');
		$table->foreign('CONTACT')
		      ->references('NO_ASP')
		      ->on('contacts');
		$table->text('LANGUES');
		$table->foreign('LANGUES')
		      ->references('LANGUE')
		      ->on('langues');
		$table->integer('AGENCE');
		$table->foreign('AGENCE')
		      ->references('NO_ASP')
		      ->on('agences');
		
		//$table->integer('PIECE');
		//$table->foreign('PIECE')
		//      ->references('NO_ASP')
		//      ->on('pieces');
		//$table->text('ATTRIBUTS');
		//$table->foreign('ATTRIBUTS')
                //      ->references('ATTRIBUT')
                //      ->on('attributs');
		//$table->integer('PHOTOS');
		//$table->foreign('PHOTOS')
		//      ->references('NO_ASP')
		//      ->on('photos');

		$table->string('QUARTIER',25);
		$table->string('RESIDENCE',25);
		$table->string('TRANSPORT',25);
		$table->string('PROXIMITE',25);
		$table->string('SITUATION',25);
		$table->string('MODE_CHAUFF',25);
		$table->string('TYPE_CHAUFF',25);
		$table->string('NAT_CHAUFF',25);
		$table->string('TYPE_CUIS',25);
		$table->string('COUV',25);
		$table->string('FACADE',25);
		$table->string('ETAT_EXT',25);
		$table->string('EAU_CHAUDE',25);
		$table->string('DIGICODE',10);
		$table->string('CE',2);
		$table->string('STANDING',25);
		$table->string('TYPE_CONS',25);
		$table->float('TAXE_HAB');
		$table->string('REF_ADB',25);
		$table->string('REGIME_BAIL',25);
		$table->string('TYPE_CAUTION',25);
		$table->float('LOYER_HC');
		$table->float('LOYER_PARK');
		$table->float('PROV_CH');
		$table->float('PROV_CHAUFF');
		$table->float('FRAIS_DIV');
		$table->float('LOYER_CC');
		$table->float('TRAVAUX');
		$table->float('DEP_G');
		$table->float('HRAL');
		$table->float('SURF_HAB');
		$table->float('HRAP');
		$table->float('SURF_JAR');
		$table->integer('NB_PCE');
		$table->integer('NB_CHB');
		$table->integer('ETAGE');
		$table->integer('NB_ETAGE');
		$table->integer('NB_WC');
		$table->integer('NB_SDB');
		$table->integer('NB_SE');
		$table->integer('NB_PARK_INT');
		$table->integer('NB_PARK_EXT');
		$table->integer('NB_BOX');
		$table->integer('ANNEE_CONS');
		$table->integer('NB_CAVE');
		$table->integer('NB_BALCON');
		$table->boolean('ASCE');
		$table->boolean('HAND');
		$table->boolean('INTERPHONE');
		$table->boolean('HAS_DIGICODE');
		$table->boolean('CAUTION');
		$table->boolean('OCC_PROP');
		$table->boolean('PISCINE');
		$table->boolean('MEUBLE');
		$table->date('DATE_LIBER');
		$table->date('DATE_DISP');
		$table->date('DATE_FIN_BAIL');
		$table->string('ETAT_INT',25);
		$table->integer('NB_TERR');
		$table->integer('NB_NIV');
		$table->integer('DUREE_BAIL');
		$table->float('SURF_SEJ');
		$table->integer('NON_DPE');
		$table->float('HONO_LOC');
		$table->integer('MANDAT_ALG');
		$table->date('DATE_DPE');
		$table->date('DATE_ERNT');
		$table->date('DATE_PB');
		$table->string('PRESTA_DPE',25);
		$table->string('PRESTA_ERNT',25);
		$table->string('PRESTA_PB',25);
		$table->float('TOTAL_HONO_TTC');
		$table->string('DPE',1);
		$table->string('GES',1);
		$table->float('VAL_DPE');
		$table->float('VAL_GES');
		$table->float('TOTAL_HONO_HT');
		$table->boolean('ALI');
		$table->integer('INDICE_NATURE_CHARGE');
		$table->integer('COMPLEMENT_LOYER');
		$table->integer('INDICE_REGLEMENT_LOYER');
		$table->integer('INDICE_REGLEMENT_CHARGES');
		$table->float('HONO_ETAT_LIEU_LOC');
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
