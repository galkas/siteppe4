<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnnoncesType21 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annoncestype21s',function($table) {
		$table->integer('NO_ASP');
		$table->primary('NO_ASP');
		$table->foreign('NO_ASP')
		      ->references('NO_ASP')
		      ->on('annonces');
		$table->timestamps();
		$table->integer('id');
		$table->string('CODE_SOCIETE',4);
		$table->string('CODE_SITE',2);
		$table->string('NO_DOSSIER',5);
		$table->string('NO_RUE',10);
		$table->string('TYPE_RUE',15);
		$table->string('NO_MANDAT',10);
		$table->string('TYPE_MANDAT',15);
		$table->string('ADR',32);
		$table->string('SUITE_ADR',32);
		$table->string('CP',5);
		$table->string('VILLE',26);
		$table->string('PAYS',25);
		$table->string('QUARTIER',25);
		$table->string('RESIDENCE',25);
		$table->string('TRANSPORT',25);
		$table->string('PROXIMITE',25);
		$table->string('SITUATION',25);
		$table->string('NOM_PROM',40);
		$table->string('NOM_PRG',32);
		$table->string('URL_VIDEO',150);
		$table->string('TYPE1',25);
		$table->string('TYPE2',25);
		$table->string('TYPE3',25);
		$table->string('TYPE4',25);
		$table->float('PRIX_MIN');
		$table->float('TX_HONO');
		$table->float('PRIX_MAX');
		$table->integer('TYPE_BIEN');
		$table->integer('ETAT_AVANC');
		$table->integer('PCE_MIN');
		$table->integer('PCE_MAX');
		$table->integer('COLLIND');
		$table->integer('NB_TYPE1');
		$table->integer('NB_TYPE2');
		$table->integer('NB_TYPE3');
		$table->integer('NB_TYPE4');
		$table->integer('NB_TYPE5');
		$table->integer('NB_TYPE6');
		$table->boolean('CC');
		$table->date('DATE_CREATION');
		$table->date('DATE_MODIF');
		$table->date('DATE_MAND');
		$table->date('DATE_FIN_MAND');
		$table->string('TXT_INTERNET',1024);
		$table->string('TYPE5',25);
		$table->string('TYPE6',25);
		$table->string('SECTEUR',100);
		$table->string('CRIT_COMP',512);
		$table->boolean('PRIX_CONF');
		$table->text('MOTS_CLES');
		$table->text('CANAUX');
		$table->text('FILTRES');
		$table->text('LOTNEUFS');
		$table->integer('CONTACT');
		$table->foreign('CONTACT')
		      ->references('NO_ASP')
		      ->on('contacts');
		$table->text('LANGUES');
		$table->foreign('LANGUES')
		      ->references('LANGUE')
		      ->on('langues');
		$table->integer('AGENCE');
		$table->foreign('AGENCE')
		      ->references('NO_ASP')
		      ->on('agences');
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
