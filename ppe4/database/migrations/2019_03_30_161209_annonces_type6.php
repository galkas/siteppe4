<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnnoncesType6 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annoncestype6s',function($table) {
		$table->integer('NO_ASP');
		$table->primary('NO_ASP');
		$table->foreign('NO_ASP')
		      ->references('NO_ASP')
		      ->on('annonces');
		$table->timestamps();
		$table->integer('id');
		$table->string('CODE_SOCIETE',4);
		$table->string('CODE_SITE',2);
		$table->string('NO_DOSSIER',5);
		$table->string('NO_RUE',10);
		$table->string('TYPE_RUE',15);
		$table->string('NO_MANDAT',10);
		$table->string('TYPE_MANDAT',15);
		$table->string('ADR',32);
		$table->string('SUITE_ADR',32);
		$table->string('CP',5);
		$table->string('VILLE',26);
		$table->string('PAYS',25);
		$table->string('CP_WEB',5);
		$table->string('VILLE_WEB',26);
		$table->string('CAT',25);
		$table->string('URL_VIDEO',150);
		$table->integer('TYPE_BIEN');
		$table->integer('ETAT_AVANC');
		$table->boolean('CC');
		$table->date('DATE_CREATION');
		$table->date('DATE_MODIF');
		$table->date('DATE_MAND');
		$table->date('DATE_FIN_MAND');
		$table->string('TXT_INTERNET',1024);
		$table->string('SECTEUR',100);
		$table->string('CRIT_COMP',512);
		$table->date('DATE_INIT_PH');
		$table->string('NOM_CONTACT',25);
		$table->string('TEL_CONTACT',20);
		$table->string('DIST',20);
		$table->float('LAT');
		$table->float('LON');
		$table->boolean('PRIX_CONF');
		$table->text('MOTS_CLES');
		$table->integer('CONTACT');
		$table->foreign('CONTACT')
		      ->references('NO_ASP')
		      ->on('contacts');
		$table->text('LANGUES');
		$table->foreign('LANGUES')
		      ->references('LANGUE')
		      ->on('langues');
		$table->integer('AGENCE');
		$table->foreign('AGENCE')
		      ->references('NO_ASP')
		      ->on('agences');
		
		//$table->integer('PIECE');
		//$table->foreign('PIECE')
		//      ->references('NO_ASP')
		//      ->on('pieces');
		//$table->text('ATTRIBUTS');
		//$table->foreign('ATTRIBUTS')
                //      ->references('ATTRIBUT')
                //      ->on('attributs');
		//$table->integer('PHOTOS');
		//$table->foreign('PHOTOS')
		//      ->references('NO_ASP')
		//      ->on('photos');

		$table->string('REG_FISC',25);
		$table->string('FORM_JUR',25);
		$table->string('ACTIVITE',25);
		$table->string('RAISON_SOC',25);
		$table->string('ENSEIGNE',25);
		$table->string('TYPE_BAIL',25);
		$table->string('DIGICODE',10);
		$table->float('PV');
		$table->float('HONO');
		$table->float('TX_HONO');
		$table->float('CAP_SOC');
		$table->float('TAXE_PROF');
		$table->float('MAS_SAL');
		$table->float('CHARGES');
		$table->float('LOYER');
		$table->float('SURF_PROF');
		$table->float('SURF_ANNEXE');
		$table->float('SURF_TERR');
		$table->integer('NB_PCE');
		$table->integer('NB_EMP');
		$table->integer('ANNEE1');
		$table->integer('ANNEE2');
		$table->integer('ANNEE3');
		$table->integer('NB_PARK_INT');
		$table->integer('NB_PARK_EXT');
		$table->integer('NB_BOX');
		$table->date('DATE_CREAT_FDC');
		$table->date('DATE_MODIF_PRIX');
		$table->string('TXT_PRESSE',1024);
		$table->float('LON_VIT');
		$table->float('SURF_LOG');
		$table->float('TAUX_TAXE_PROF');
		$table->float('CA_AN1');
		$table->float('CA_AN2');
		$table->float('CA_AN3');
		$table->float('BIC_AN1');
		$table->float('BIC_AN2');
		$table->float('BIC_AN3');
		$table->float('SAL_EXPLOIT');
		$table->float('VAL_STOCK');
		$table->float('CASH_FLOW');
		$table->integer('NON_DPE');
		$table->string('NO_PRIV',10);
		$table->date('DATE_FIN_BAIL');
		$table->integer('CH_HONO');
		$table->string('DPE',1);
		$table->string('GES',1);
		$table->float('VAL_DPE');
		$table->float('VAL_GES');
		$table->boolean('CPG_LAF');
		$table->integer('COPROPRIETE');
		$table->integer('NB_LOTS_COPRO');
		$table->float('MONTANT_QUOTEPART');
		$table->integer('PROCEDURE_SYND');
		$table->string('PROCEDURE_DETAIL',128);
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
