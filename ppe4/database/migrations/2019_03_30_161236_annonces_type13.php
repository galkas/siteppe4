<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnnoncesType13 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annoncestype13s',function($table) {
		$table->integer('NO_ASP');
		$table->primary('NO_ASP');
		$table->foreign('NO_ASP')
		      ->references('NO_ASP')
		      ->on('annonces');
		$table->timestamps();
		$table->integer('id');
		$table->string('CODE_SOCIETE',4);
		$table->string('CODE_SITE',2);
		$table->string('NO_DOSSIER',5);
		$table->string('NO_RUE',10);
		$table->string('TYPE_RUE',15);
		$table->string('NO_MANDAT',10);
		$table->string('TYPE_MANDAT',15);
		$table->string('ADR',32);
		$table->string('SUITE_ADR',32);
		$table->string('CP',5);
		$table->string('VILLE',26);
		$table->string('PAYS',25);
		$table->string('CP_WEB',5);
		$table->string('VILLE_WEB',26);
		$table->string('CAT',25);
		$table->string('URL_VIDEO',150);
		$table->integer('TYPE_BIEN');
		$table->integer('ETAT_AVANC');
		$table->boolean('CC');
		$table->date('DATE_CREATION');
		$table->date('DATE_MODIF');
		$table->date('DATE_MAND');
		$table->date('DATE_FIN_MAND');
		$table->string('TXT_INTERNET',1024);
		$table->string('SECTEUR',100);
		$table->string('CRIT_COMP',512);
		$table->date('DATE_INIT_PH');
		$table->string('NOM_CONTACT',25);
		$table->string('TEL_CONTACT',20);
		$table->string('DIST',20);
		$table->float('LAT');
		$table->float('LON');
		$table->boolean('PRIX_CONF');
		$table->text('MOTS_CLES');
		$table->integer('CONTACT');
		$table->foreign('CONTACT')
		      ->references('NO_ASP')
		      ->on('contacts');
		$table->text('LANGUES');
		$table->foreign('LANGUES')
		      ->references('LANGUE')
		      ->on('langues');
		$table->integer('AGENCE');
		$table->foreign('AGENCE')
		      ->references('NO_ASP')
		      ->on('agences');
		
		//$table->integer('PIECE');
		//$table->foreign('PIECE')
		//      ->references('NO_ASP')
		//      ->on('pieces');
		//$table->text('ATTRIBUTS');
		//$table->foreign('ATTRIBUTS')
                //      ->references('ATTRIBUT')
                //      ->on('attributs');
		//$table->integer('PHOTOS');
		//$table->foreign('PHOTOS')
		//      ->references('NO_ASP')
		//      ->on('photos');
		//$table->integer('SURFACE');
		//$table->foreign('SURFACE')
		//      ->references('NO_ASP')
		//      ->on('surfaces');

		$table->string('TBL',25);
		$table->string('DEP_G',25);
		$table->string('MODE_CHAUFF',25);
		$table->string('NOM_GEST',32);
		$table->string('REG_FISC',25);
		$table->string('TYPE_CHAUFF',25);
		$table->string('NAT_CHAUFF',25);
		$table->string('COUV',25);
		$table->string('FACADE',25);
		$table->string('ETAT_EXT',25);
		$table->string('STANDING',25);
		$table->string('TYPE_CONS',25);
		$table->float('TX_HONO');
		$table->float('SURF_TERR');
		$table->float('SURF_TOT');
		$table->float('SURF_MINI');
		$table->integer('NB_ETAGE');
		$table->integer('NB_PARK_INT');
		$table->integer('NB_PARK_EXT');
		$table->integer('NB_BOX');
		$table->integer('ANNEE_CONS');
		$table->boolean('ASCE');
		$table->boolean('HAND');
		$table->boolean('INTERPHONE');
		$table->boolean('DIGICODE');
		$table->string('REGL_LOYER',30);
		$table->float('LAM_MIN');
		$table->float('LAM_MAX');
		$table->float('F_HONO');
		$table->float('CMA_MIN');
		$table->float('CMA_MAX');
		$table->float('LOYER_AN_HTHC');
		$table->float('TBMA');
		$table->float('TFMA');
		$table->float('TPIA');
		$table->float('LPEA');
		$table->float('REPRISE');
		$table->boolean('U_BUR');
		$table->boolean('U_COMM');
		$table->boolean('U_ACT');
		$table->boolean('U_PROF');
		$table->boolean('MC');
		$table->boolean('TELESURV');
		$table->date('DATE_LIBER');
		$table->date('DATE_CONGE');
		$table->date('DATE_BAIL');
		$table->date('DATE_DISP');
		$table->string('ETAT_INT',25);
		$table->integer('NB_NIV');
		$table->float('TX_TAXE_PROF');
		$table->float('LON_VIT');
		$table->float('CDAB');
		$table->boolean('GARDIENNAGE');
		$table->boolean('PREST');
		$table->boolean('IMM_COL');
		$table->boolean('IMM_INDEP');
		$table->boolean('CLIM');
		$table->boolean('RIE');
		$table->boolean('IFB');
		$table->boolean('NET_TAXE');
		$table->boolean('NO_CDAB');
		$table->string('DESSERTE',1024);
		$table->float('HSP');
		$table->float('SURF_BUR');
		$table->integer('CH_HONO');
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
