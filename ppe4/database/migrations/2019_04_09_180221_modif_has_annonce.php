<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifHasAnnonce extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modifhasannonces',function($table) {
		$table->increments('ID');
		$table->timestamps();
		$table->integer('id');
		$table->integer('ANNONCE');
		$table->foreign('ANNONCE')
		      ->references('NO_ASP')
		      ->on('annonces');
		$table->integer('MODIF');
		$table->foreign('MODIF')
		      ->references('NO_MODIF')
		      ->on('modifs');
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
