<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnnoncesHasAttribut extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annonceshasattributs',function($table) {
		$table->increments('ID');
		$table->timestamps();
		$table->integer('id');
		$table->integer('ANNONCE');
		$table->foreign('ANNONCE')
		      ->references('NO_ASP')
		      ->on('annonces');
		$table->string('ATTRIBUT');
		$table->foreign('ATTRIBUT')
		      ->references('ATTRIBUT')
		      ->on('attributs');
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
