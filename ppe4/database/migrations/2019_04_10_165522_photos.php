<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Photos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos',function($table) {
		$table->integer('NO_ASP');
		$table->primary('NO_ASP');
		$table->integer('NO_ASP_ANNONCE');
		$table->foreign('NO_ASP_ANNONCE')
		      ->references('NO_ASP')
		      ->on('annonces');
		$table->timestamps();
		$table->integer('id');
		$table->string('LETTRE',1);
		$table->date('DATE');
		$table->string('URL');
		$table->string('DESC');
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
