<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnnoncesType14 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annoncestype14s',function($table) {
		$table->integer('NO_ASP');
		$table->primary('NO_ASP');
		$table->foreign('NO_ASP')
		      ->references('NO_ASP')
		      ->on('annonces');
		$table->timestamps();
		$table->integer('id');
		$table->string('CODE_SOCIETE',4);
		$table->string('CODE_SITE',2);
		$table->string('NO_DOSSIER',5);
		$table->string('NO_RUE',10);
		$table->string('TYPE_RUE',15);
		$table->string('NO_MANDAT',10);
		$table->string('TYPE_MANDAT',15);
		$table->string('ADR',32);
		$table->string('SUITE_ADR',32);
		$table->string('CP',5);
		$table->string('VILLE',26);
		$table->string('PAYS',25);
		$table->string('CP_WEB',5);
		$table->string('VILLE_WEB',26);
		$table->string('CAT',25);
		$table->string('URL_VIDEO',150);
		$table->integer('TYPE_BIEN');
		$table->integer('ETAT_AVANC');
		$table->boolean('CC');
		$table->date('DATE_CREATION');
		$table->date('DATE_MODIF');
		$table->date('DATE_MAND');
		$table->date('DATE_FIN_MAND');
		$table->string('TXT_INTERNET',1024);
		$table->string('SECTEUR',100);
		$table->string('CRIT_COMP',512);
		$table->date('DATE_INIT_PH');
		$table->string('NOM_CONTACT',25);
		$table->string('TEL_CONTACT',20);
		$table->string('DIST',20);
		$table->float('LAT');
		$table->float('LON');
		$table->boolean('PRIX_CONF');
		$table->text('MOTS_CLES');
		$table->integer('CONTACT');
		$table->foreign('CONTACT')
		      ->references('NO_ASP')
		      ->on('contacts');
		$table->text('LANGUES');
		$table->foreign('LANGUES')
		      ->references('LANGUE')
		      ->on('langues');
		$table->integer('AGENCE');
		$table->foreign('AGENCE')
		      ->references('NO_ASP')
		      ->on('agences');
		
		//$table->integer('PIECE');
		//$table->foreign('PIECE')
		//      ->references('NO_ASP')
		//      ->on('pieces');
		//$table->text('ATTRIBUTS');
		//$table->foreign('ATTRIBUTS')
                //      ->references('ATTRIBUT')
                //      ->on('attributs');
		//$table->integer('PHOTOS');
		//$table->foreign('PHOTOS')
		//      ->references('NO_ASP')
		//      ->on('photos');

		$table->string('QUARTIER',25);
		$table->string('NOM_PARK',25);
		$table->string('TRANSPORT',25);
		$table->string('PROXIMITE',25);
		$table->string('SITUATION',25);
		$table->string('TYPE_CONS',25);
		$table->string('ETAT_GEN',25);
		$table->string('DIGICODE',10);
		$table->string('REF_ADB',25);
		$table->float('LOYER_HC');
		$table->float('PROV_CH');
		$table->float('TVA');
		$table->float('LOYER_TTC');
		$table->float('CAUTION_BIP');
		$table->float('DEP_G');
		$table->float('HRATTC');
		$table->float('SURF');
		$table->float('TX_TVA');
		$table->integer('NIVEAU');
		$table->integer('NB_NIV');
		$table->integer('TYPE_STAT');
		$table->integer('TYPE_ACCES');
		$table->integer('ACCES_PIETON');
		$table->integer('NB_VOITURE');
		$table->integer('ANNEE_CONS');
		$table->integer('LOYER_MT');
		$table->boolean('ASCE');
		$table->boolean('HAND');
		$table->boolean('BIP');
		$table->boolean('CARTE_MAGN');
		$table->boolean('VIDEO_SURV');
		$table->date('DATE_LIBER');
		$table->date('DATE_DISP');
		$table->date('DATE_FIN_BAIL');
		$table->integer('DUREE_BAIL');
		$table->float('LONGUEUR');
		$table->float('LARGEUR');
		$table->float('HAUTEUR');
		$table->boolean('GARDIENNAGE');
		$table->float('HONO_LOC_TTC');
		$table->float('TOT_HONO_LOC_TTC');
		$table->integer('CH_MT');
		$table->integer('INDICE_NATURE_CHARGE');
		$table->integer('INDICE_REGLEMENT_LOYER');
		$table->integer('INDICE_REGLEMENT_CHARGES');
		$table->float('HONO_ETAT_LIEU_LOC');
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
