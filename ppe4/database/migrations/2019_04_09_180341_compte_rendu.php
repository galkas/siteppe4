<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CompteRendu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compterendus',function($table) {
		$table->integer('NO_COMPTE_RENDU');
		$table->primary('NO_COMPTE_RENDU');
		$table->timestamps();
		$table->integer('id');
		$table->date('DATE_VISITE');
		$table->string('PROPRIETAIRE',50);
		$table->integer('ANNONCE');
		$table->foreign('ANNONCE')
		      ->references('NO_ASP')
		      ->on('annonces');
		$table->string('VISITEURS',50);
		$table->string('REMARQUE',150);
		$table->string('FONCTION_VISITEUR',50);
		$table->string('FINANCEMENT_PREVU',60);
		$table->integer('NOTE_VISITEUR');
		$table->string('SYNTHESE',1024);
		$table->integer('CONTACT');
		$table->foreign('CONTACT')
		      ->references('NO_ASP')
		      ->on('contacts');
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
