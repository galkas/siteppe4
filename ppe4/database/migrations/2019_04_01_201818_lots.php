<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Lots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lots',function($table) {
		$table->integer('NO_ASP');
		$table->primary('NO_ASP');
		$table->timestamps();
		$table->integer('id');
		$table->foreign('NO_ASP')
		      ->references('NO_ASP')
		      ->on('annonces');
		$table->integer('ETAGE');
		$table->float('LOYER_ANN');
		$table->float('SURFACE');
		$table->string('NO_ORDRE',2);
		$table->string('COMMENTAIRES',30);
		$table->string('BAIL',15);
		$table->string('NO_LOT',15);
		$table->string('CAT_APPT',40);
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
