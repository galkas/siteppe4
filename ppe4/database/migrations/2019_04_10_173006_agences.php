<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Agences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agences',function($table) {
		$table->integer('NO_ASP');
		$table->primary('NO_ASP');
		$table->timestamps();
		$table->integer('id');
		$table->string('CODE_SOCIETE',4);
		$table->string('CODE_SITE',2);
		$table->string('RAISON_SOCIALE',32);
		$table->string('ENSEIGNE',32);
		$table->string('CIVILITE',15);
		$table->string('NOM',32);
		$table->string('PRENOM',20);
		$table->string('ADRESSE',32);
		$table->string('SUITE_ADRESSE',32);
		$table->string('CP',5);
		$table->string('VILLE',26);
		$table->string('PAYS',25);
		$table->string('TEL',20);
		$table->string('FAX',20);
		$table->string('MOBILE',20);
		$table->string('MAIL',50);
		$table->string('FORME_JUR',5);
		$table->string('SIRET',40);
		$table->string('NO_CARTEPRO_T',10);
		$table->string('NO_CARTEPRO_G',10);
		$table->string('GROUPE',25);
		$table->string('SYNDICAT',25);
		$table->string('CAISSE_GARENTIE',25);
		$table->float('CAPITAL_SOCIAL');
		$table->float('MONTANT_GARENTIE_T');
		$table->float('MONTANT_GARENTIE_G');
		$table->date('DATE_CREAT');
		$table->date('DATE_MODIF');
		$table->string('RESEAU',25);
		$table->string('WEB',50);
		$table->string('INFO_JUR',1024);
		$table->string('SECONDE_DEVISE',5);
		$table->float('TX_CONV_SEC_DEVISE');
		$table->string('TEL_LOC',20);
		$table->string('MAIL_LOC',50);
		$table->string('PREF_CARTE_PRO',26);
		$table->string('ADR_SS',32);
		$table->string('SUITE_ADR_SS',32);
		$table->string('CP_SS',5);
		$table->string('VILLE_SS',26);
		$table->string('PAYS_SS',25);
		$table->string('ADR_GARANT',32);
		$table->string('SUITE_ADR_GARANT',32);
		$table->string('CP_GARANT',5);
		$table->string('VILLE_GARANT',26);
		$table->boolean('GARENTIE_MF');
		$table->integer('TYPE_CARTE_PRO');
		$table->integer('TYPE_GARENTIE');
		$table->string('NO_TVA_INTRA',25);
		$table->string('ADR_PREF',32);
		$table->string('SUITE_ADR_PREF',32);
		$table->string('CP_PREF',5);
		$table->string('VILLE_PREF',26);
		$table->float('LAT');
		$table->float('LON');
		$table->integer('ID_TIERS');
		$table->string('NOM_GARENTIE_G',25);
		$table->string('ADR_GARENTIE_G',32);
		$table->string('SUITE_ADR_GARENTIE_G',32);
		$table->string('CP_GARENTIE_G',5);
		$table->string('VILLE_GARENTIE_G',26);
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
