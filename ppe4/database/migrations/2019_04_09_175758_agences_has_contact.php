<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgencesHasContact extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agenceshascontacts',function($table) {
		$table->increments('ID');
		$table->timestamps();
		$table->integer('id');
		$table->integer('AGENCE');
		$table->foreign('AGENCE')
		      ->references('NO_ASP')
		      ->on('agences');
		$table->integer('CONTACT');
		$table->foreign('CONTACT')
		      ->references('NO_ASP')
		      ->on('contacts');
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
