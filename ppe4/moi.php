<?php
	
	function connexionPDO() {
		$login = "postgres";
		$mdp = "postgres";
		$bdd = "ppe4";
		$server = "localhost";

		try{
			$conn = new PDO("pgsql:host=$server;dbname=$bdd", $login, $mdp);
			return $conn;
		}catch(PDOException $e) {
			print "Erreur de connection PDO";
			die();
		}
	}
?>
