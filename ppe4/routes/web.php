<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Accueil
Route::get('/', function () {
    return view('auth/login');
});
Route::get('/Accueil', function () {
	return view('Accueil');
});
Route::get('/home', function () {
	return view('Accueil');
});


//consultation bien solo
Route::get('/ConsultationSolo/AfficheSoloType1', function () {
    return view('ConsultationSolo/AfficheSoloType1');
});
Route::post('/ConsultationSolo/AfficheSoloType1', function () {
    return view('ConsultationSolo/AfficheSoloType1');
});

Route::get('/ConsultationSolo/AfficheSoloType2', function () {
    return view('ConsultationSolo/AfficheSoloType2');
});
Route::post('/ConsultationSolo/AfficheSoloType2', function () {
    return view('ConsultationSolo/AfficheSoloType2');
});

Route::get('/ConsultationSolo/AfficheSoloType3', function () {
    return view('ConsultationSolo/AfficheSoloType3');
});
Route::post('/ConsultationSolo/AfficheSoloType3', function () {
    return view('ConsultationSolo/AfficheSoloType3');
});

Route::get('/ConsultationSolo/AfficheSoloType4', function () {
    return view('ConsultationSolo/AfficheSoloType4');
});
Route::post('/ConsultationSolo/AfficheSoloType4', function () {
    return view('ConsultationSolo/AfficheSoloType4');
});

Route::get('/ConsultationSolo/AfficheSoloType5', function () {
    return view('ConsultationSolo/AfficheSoloType5');
});
Route::post('/ConsultationSolo/AfficheSoloType5', function () {
    return view('ConsultationSolo/AfficheSoloType5');
});

Route::get('/ConsultationSolo/AfficheSoloType6', function () {
    return view('ConsultationSolo/AfficheSoloType6');
});
Route::post('/ConsultationSolo/AfficheSoloType6', function () {
    return view('ConsultationSolo/AfficheSoloType6');
});

Route::get('/ConsultationSolo/AfficheSoloType7', function () {
    return view('ConsultationSolo/AfficheSoloType7');
});
Route::post('/ConsultationSolo/AfficheSoloType7', function () {
    return view('ConsultationSolo/AfficheSoloType7');
});

Route::get('/ConsultationSolo/AfficheSoloType11', function () {
    return view('ConsultationSolo/AfficheSoloType11');
});
Route::post('/ConsultationSolo/AfficheSoloType11', function () {
    return view('ConsultationSolo/AfficheSoloType11');
});

Route::get('/ConsultationSolo/AfficheSoloType12', function () {
    return view('ConsultationSolo/AfficheSoloType12');
});
Route::post('/ConsultationSolo/AfficheSoloType12', function () {
    return view('ConsultationSolo/AfficheSoloType12');
});

Route::get('/ConsultationSolo/AfficheSoloType13', function () {
    return view('ConsultationSolo/AfficheSoloType13');
});
Route::post('/ConsultationSolo/AfficheSoloType13', function () {
    return view('ConsultationSolo/AfficheSoloType13');
});

Route::get('/ConsultationSolo/AfficheSoloType14', function () {
    return view('ConsultationSolo/AfficheSoloType14');
});
Route::post('/ConsultationSolo/AfficheSoloType14', function () {
    return view('ConsultationSolo/AfficheSoloType14');
});

Route::get('/ConsultationSolo/AfficheSoloType21', function () {
    return view('ConsultationSolo/AfficheSoloType21');
});
Route::post('/ConsultationSolo/AfficheSoloType21', function () {
    return view('ConsultationSolo/AfficheSoloType21');
});

Route::get('/ConsultationSolo/AfficheSoloTypeLotsNeufs', function () {
    return view('ConsultationSolo/AfficheSoloTypeLotsNeufs');
});
Route::post('/ConsultationSolo/AfficheSoloTypeLotsNeufs', function () {
    return view('ConsultationSolo/AfficheSoloTypeLotsNeufs');
});
//consultation biens
Route::get('/AfficheAnnonceType1', function () {
    return view('Consultation/AfficheAnnonceType1');
});
Route::get('/AfficheAnnonceType2', function () {
    return view('Consultation/AfficheAnnonceType2');
});
Route::get('/AfficheAnnonceType3', function () {
    return view('Consultation/AfficheAnnonceType3');
});
Route::get('/AfficheAnnonceType4', function () {
    return view('Consultation/AfficheAnnonceType4');
});
Route::get('/AfficheAnnonceType5', function () {
    return view('Consultation/AfficheAnnonceType5');
});
Route::get('/AfficheAnnonceType6', function () {
    return view('Consultation/AfficheAnnonceType6');
});
Route::get('/AfficheAnnonceType7', function () {
    return view('Consultation/AfficheAnnonceType7');
});
Route::get('/AfficheAnnonceType11', function () {
    return view('Consultation/AfficheAnnonceType11');
});
Route::get('/AfficheAnnonceType12', function () {
    return view('Consultation/AfficheAnnonceType12');
});
Route::get('/AfficheAnnonceType13', function () {
    return view('Consultation/AfficheAnnonceType13');
});
Route::get('/AfficheAnnonceType14', function () {
    return view('Consultation/AfficheAnnonceType14');
});
Route::get('/AfficheAnnonceType21', function () {
    return view('Consultation/AfficheAnnonceType21');
});
Route::get('/AfficheLotsNeufs', function () {
    return view('Consultation/AfficheAnnonceTypeLotsNeufs');
});
//Ajout Biens
Route::get('/AjoutAnnonceType1', function () {
    return view('Ajout/AjoutBienType1');
})->middleware('auth');
Route::post('/AjoutAnnonceType1', function () {
	$previous = new App\annonce;
	$previous->NO_ASP = request('noASP');
	$previous->id = 1;
	
	$previous->save();
	
	$Annonce = new App\annoncestype1;
	$Annonce->NO_ASP = request('noASP');
	$Annonce->CODE_SOCIETE = request('code_societe');
	$Annonce->CODE_SITE = request('code_site');
	$Annonce->NO_DOSSIER = request('no_dossier');
	$Annonce->NO_RUE = request('no_rue');
	$Annonce->TYPE_RUE = request('type_rue');
	$Annonce->NO_MANDAT = request('no_mandat');
	$Annonce->TYPE_MANDAT = request('type_mandat');
	$Annonce->ADR = request('adr');
	$Annonce->SUITE_ADR = request('suite_adr');
	$Annonce->CP = request('cp');
	$Annonce->VILLE = request('ville');
	$Annonce->PAYS = request('pays');
	$Annonce->QUARTIER = request('quartier');
	$Annonce->RESIDENCE = request('residence');
	$Annonce->TRANSPORT = request('transport');
	$Annonce->PROXIMITE = request('proximite');
	$Annonce->SITUATION = request('situation');
	$Annonce->CP_WEB = request('cp_web');
	$Annonce->VILLE_WEB = request('ville_web');
	$Annonce->MODE_CHAUFF = request('mode_chauff');
	$Annonce->REG_FISC = request('reg_fisc');
	$Annonce->FORM_JUR = request('forme_jur');
	$Annonce->CAT = request('cat');
	$Annonce->TYPE_CHAUFF = request('type_chauff');
	$Annonce->NAT_CHAUFF = request('nat_chauff');
	$Annonce->TYPE_CUIS = request('type_cuis');
	$Annonce->STANDING = request('standing');
	$Annonce->TYPE_CONS = request('type_cons');
	$Annonce->COUV = request('couv');
	$Annonce->FACADE = request('facade');
	$Annonce->ETAT_EXT = request('etat_ext');
	$Annonce->EAU_CHAUDE = request('eau_chaude');
	$Annonce->HAS_DIGICODE = request('digicode');
	$Annonce->CERTIF_CAR = request('certif_car');
	$Annonce->CERTIF_AM = request('certif_am');
	$Annonce->CERTIF_PB = request('certif_pb');
	$Annonce->CERTIF_TER = request('certif_ter');
	$Annonce->CE = request('ce');
	$Annonce->URL_VIDEO = request('url_video');
	$Annonce->PV = request('pv');
	$Annonce->HONO = request('hono');
	$Annonce->TX_HONO = request('tx_hono');
	$Annonce->TRAVAUX = request('travaux');
	$Annonce->TAXE_HAB = request('taxe_hab');
	$Annonce->TAXE_FONC = request('taxe_fonc');
	$Annonce->CHARGES = request('charges');
	$Annonce->LOYER = request('loyer');
	$Annonce->SURF_HAB = request('surf_hab');
	$Annonce->SURF_CARR = request('surf_carr');
	$Annonce->SURF_JAR = request('surf_jar');
	$Annonce->TYPE_BIEN = request('type_bien');
	$Annonce->ETAT_AVANC = request('etat_avanc');
	$Annonce->NB_PCE = request('nb_pce');
	$Annonce->NB_CHB = request('nb_chb');
	$Annonce->ETAGE = request('etage');
	$Annonce->NB_ETAGE = request('nb_etage');
	$Annonce->NB_WC = request('nb_wc');
	$Annonce->NB_SDB = request('nb_sdb');
	$Annonce->NB_SE = request('nb_se');
	$Annonce->NB_PARK_INT = request('nb_park_int');
	$Annonce->NB_PARK_EXT = request('nb_park_ext');
	$Annonce->NB_BOX = request('nb_box');
	$Annonce->ANNEE_CONS = request('annee_cons');
	$Annonce->DISPO = request('dispo');
	$Annonce->NB_CAVE = request('nb_cave');
	$Annonce->NB_BALCON = request('nb_balcon');
	$Annonce->ASCE = request('asce');
	$Annonce->HAND = request('hand');
	$Annonce->INTERPHONE = request('interphone');
	$Annonce->DIGICODE = request('digicode');
	$Annonce->CC = request('cc');
	$Annonce->PISCINE = request('piscine');
	$Annonce->MEUBLE = request('meuble');
	$Annonce->DATE_CREATION = request('date_cration');
	$Annonce->DATE_MODIF = request('date_modif');
	$Annonce->DATE_MAND = request('date_mand');
	$Annonce->DATE_FIN_MAND = request('date_fin_mand');
	$Annonce->DATE_LIBER = request('date_liber');
	$Annonce->DATE_MODIF_PRIX = request('date_modif_prix');
	$Annonce->DATE_CAR = request('date_car');
	$Annonce->DATE_AM = request('date_am');
	$Annonce->DATE_PB = request('date_pb');
	$Annonce->DATE_TER = request('date_ter');
	$Annonce->TXT_INTERNET = request('txt_internet');
	$Annonce->TXT_PRESSE = request('txt_presse');
	$Annonce->ETAT_INT = request('etat_int');
	$Annonce->NB_TERR = request('nb_terr');
	$Annonce->NB_NIV = request('nb_niv');
	$Annonce->NB_CHB_SERV = request('nb_chb_serv');
	$Annonce->SURF_SEJ = request('surf_sej');
	$Annonce->VIAGER_AGE1 = request('viager_age1');
	$Annonce->BOUQUET = request('bouquet');
	$Annonce->RENTE = request('rente');
	$Annonce->GARDIEN = request('gardien');
	$Annonce->PREST = request('prestige');
	$Annonce->VIAGER = request('viager');
	$Annonce->VIAGER_LIB = request('viager_libre');
	$Annonce->INSTALL_GAZ = request('install_gaz');
	$Annonce->NON_DPE = request('non_dpe');
	$Annonce->INFO_BALCON = request('info_balcon');
	$Annonce->INFO_TERR = request('info_terr');
	$Annonce->NO_PRIV = request('no_priv');
	$Annonce->SECTEUR = request('secteur');
	$Annonce->CRIT_COMP = request('crit_com');
	$Annonce->VIAGER_AGE2 = request('viager_age2');
	$Annonce->DATE_INIT_PH = request('date_init_ph');
	$Annonce->DATE_DPE = request('date_dpe');
	$Annonce->DATE_ERNT = request('date_ernt');
	$Annonce->DATE_GAZ = request('date_gaz');
	$Annonce->PRESTA_DPE = request('presta_dpe');
	$Annonce->PRESTA_ERNT = request('presta_erng');
	$Annonce->PRESTA_GAZ = request('presta_gaz');
	$Annonce->NOM_CONTACT = request('nom_contact');
	$Annonce->TEL_CONTACT = request('tel_contact');
	$Annonce->DIST = request('dist');
	$Annonce->CH_HONO = request('ch_hono');
	$Annonce->DPE = request('dpe');
	$Annonce->GES = request('ges');
	$Annonce->VAL_DPE = request('val_dpe');
	$Annonce->VAL_GES = request('val_ges');
	$Annonce->CPG_LAF = request('cpg_laf');
	$Annonce->INSTALL_ELEC = request('install_elec');
	$Annonce->DATE_ELEC = request('date_elec');
	$Annonce->PRESTA_ELEC = request('presta_elec');
	$Annonce->LAT = request('lat');
	$Annonce->LON = request('lon');
	$Annonce->DATE_DIANC = request('date_dianc');
	$Annonce->PRESTA_DIANC = request('presta_dianc');
	$Annonce->TAE = request('tae');
	$Annonce->PRIX_CONF = request('prix_conf');
	$Annonce->MOTS_CLES = request('mot_cle');
	$Annonce->CONTACT = request('contact');
	$Annonce->LANGUES = request('langue');
	$Annonce->AGENCE = request('agence');
	$Annonce->COPROPRIETE = request('copropriete');
	$Annonce->NB_LOTS_COPRO = request('nb_lots_copro');
	$Annonce->MONTANT_QUOTEPART = request('montant_quotepart');
	$Annonce->PROCEDURE_SYND = request('procedure_synd');
	$Annonce->PROCEDURE_DETAIL = request('procedure_detail');
	$Annonce->id = 1;
		
	$Annonce->save();
	
	return view('AjoutReussi');
	
});

Route::get('/AjoutAnnonceType2', function () {
    return view('Ajout/AjoutBienType2');
})->middleware('auth');
Route::post('/AjoutAnnonceType2', function () {
	$previous = new App\annonce;
	$previous->NO_ASP = request('noASP');
	$previous->id = 1;
	
	$previous->save();
	
	$Annonce = new App\annoncestype2;
	$Annonce->NO_ASP = request('noASP');
	$Annonce->CODE_SOCIETE = request('code_societe');
	$Annonce->CODE_SITE = request('code_site');
	$Annonce->NO_DOSSIER = request('no_dossier');
	$Annonce->NO_RUE = request('no_rue');
	$Annonce->TYPE_RUE = request('type_rue');
	$Annonce->NO_MANDAT = request('no_mandat');
	$Annonce->TYPE_MANDAT = request('type_mandat');
	$Annonce->ADR = request('adr');
	$Annonce->SUITE_ADR = request('suite_adr');
	$Annonce->CP = request('cp');
	$Annonce->VILLE = request('ville');
	$Annonce->PAYS = request('pays');
	$Annonce->QUARTIER = request('quartier');
	$Annonce->RESIDENCE = request('residence');
	$Annonce->TRANSPORT = request('transport');
	$Annonce->PROXIMITE = request('proximite');
	$Annonce->SITUATION = request('situation');
	$Annonce->CP_WEB = request('cp_web');
	$Annonce->VILLE_WEB = request('ville_web');
	$Annonce->MODE_CHAUFF = request('mode_chauff');
	$Annonce->REG_FISC = request('reg_fisc');
	$Annonce->FORM_JUR = request('forme_jur');
	$Annonce->CAT = request('cat');
	$Annonce->TYPE_CHAUFF = request('type_chauff');
	$Annonce->NAT_CHAUFF = request('nat_chauff');
	$Annonce->TYPE_CUIS = request('type_cuis');
	$Annonce->STANDING = request('standing');
	$Annonce->TYPE_CONS = request('type_cons');
	$Annonce->COUV = request('couv');
	$Annonce->FACADE = request('facade');
	$Annonce->ETAT_EXT = request('etat_ext');
	$Annonce->EAU_CHAUDE = request('eau_chaude');
	$Annonce->DIGICODE = request('digicode');
	$Annonce->CERTIF_CAR = request('certif_car');
	$Annonce->CERTIF_AM = request('certif_am');
	$Annonce->CERTIF_PB = request('certif_pb');
	$Annonce->CERTIF_TER = request('certif_ter');
	$Annonce->URL_VIDEO = request('url_video');
	$Annonce->PV = request('pv');
	$Annonce->HONO = request('hono');
	$Annonce->TX_HONO = request('tx_hono');
	$Annonce->TRAVAUX = request('travaux');
	$Annonce->TAXE_HAB = request('taxe_hab');
	$Annonce->TAXE_FONC = request('taxe_fonc');
	$Annonce->CHARGES = request('charges');
	$Annonce->LOYER = request('loyer');
	$Annonce->SURF_HAB = request('surf_hab');
	$Annonce->SURF_CARR = request('surf_carr');
	$Annonce->SURF_TERR = request('surf_terr');
	$Annonce->TYPE_BIEN = request('type_bien');
	$Annonce->ETAT_AVANC = request('etat_avanc');
	$Annonce->NB_PCE = request('nb_pce');
	$Annonce->NB_CHB = request('nb_chb');
	$Annonce->NB_CHB_RDC = request('nb_chb_rdc');
	$Annonce->NB_MUR_MIT = request('nb_mur_mit');
	$Annonce->NB_WC = request('nb_wc');
	$Annonce->NB_SDB = request('nb_sdb');
	$Annonce->NB_SE = request('nb_se');
	$Annonce->NB_PARK_INT = request('nb_park_int');
	$Annonce->NB_PARK_EXT = request('nb_park_ext');
	$Annonce->NB_GAR = request('nb_gar');
	$Annonce->ANNEE_CONS = request('annee_cons');
	$Annonce->DISPO = request('dispo');
	$Annonce->NB_CAVE = request('nb_cave');
	$Annonce->NB_BALCON = request('nb_balcon');
	$Annonce->LOTISS = request('lotiss');
	$Annonce->HAND = request('hand');
	$Annonce->INTERPHONE = request('interphone');
	$Annonce->ALARME = request('alarme');
	$Annonce->CC = request('cc');
	$Annonce->PISCINE = request('piscine');
	$Annonce->MEUBLE = request('meuble');
	$Annonce->DATE_CREATION = request('date_cration');
	$Annonce->DATE_MODIF = request('date_modif');
	$Annonce->DATE_MAND = request('date_mand');
	$Annonce->DATE_FIN_MAND = request('date_fin_mand');
	$Annonce->DATE_LIBER = request('date_liber');
	$Annonce->DATE_MODIF_PRIX = request('date_modif_prix');
	$Annonce->DATE_CAR = request('date_car');
	$Annonce->DATE_AM = request('date_am');
	$Annonce->DATE_PB = request('date_pb');
	$Annonce->DATE_TER = request('date_ter');
	$Annonce->TXT_INTERNET = request('txt_internet');
	$Annonce->TXT_PRESSE = request('txt_presse');
	$Annonce->ETAT_INT = request('etat_int');
	$Annonce->NB_TERR = request('nb_terr');
	$Annonce->NB_NIV = request('nb_niv');
	$Annonce->NB_CHB_SERV = request('nb_chb_serv');
	$Annonce->SURF_SEJ = request('surf_sej');
	$Annonce->SURF_DEP = request('surf_dep');
	$Annonce->SS = request('ss');
	$Annonce->VIAGER_AGE1 = request('viager_age1');
	$Annonce->BOUQUET = request('bouquet');
	$Annonce->RENTE = request('rente');	
	$Annonce->PREST = request('prestige');
	$Annonce->VIAGER = request('viager');
	$Annonce->VIAGER_LIB = request('viager_libre');
	$Annonce->INSTALL_GAZ = request('install_gaz');
	$Annonce->NON_DPE = request('non_dpe');
	$Annonce->INFO_BALCON = request('info_balcon');
	$Annonce->INFO_TERR = request('info_terr');
	$Annonce->NO_PRIV = request('no_priv');
	$Annonce->SECTEUR = request('secteur');
	$Annonce->CRIT_COMP = request('crit_com');
	$Annonce->SURF_JAR = request('surf_jar');
	$Annonce->VIAGER_AGE2 = request('viager_age2');
	$Annonce->DATE_INIT_PH = request('date_init_ph');
	$Annonce->DATE_DPE = request('date_dpe');
	$Annonce->DATE_ERNT = request('date_ernt');
	$Annonce->DATE_GAZ = request('date_gaz');
	$Annonce->PRESTA_DPE = request('presta_dpe');
	$Annonce->PRESTA_ERNT = request('presta_erng');
	$Annonce->PRESTA_GAZ = request('presta_gaz');
	$Annonce->NOM_CONTACT = request('nom_contact');
	$Annonce->TEL_CONTACT = request('tel_contact');
	$Annonce->DIST = request('dist');
	$Annonce->CH_HONO = request('ch_hono');
	$Annonce->DPE = request('dpe');
	$Annonce->GES = request('ges');
	$Annonce->VAL_DPE = request('val_dpe');
	$Annonce->VAL_GES = request('val_ges');
	$Annonce->CPG_LAF = request('cpg_laf');
	$Annonce->INSTALL_ELEC = request('install_elec');
	$Annonce->DATE_ELEC = request('date_elec');
	$Annonce->PRESTA_ELEC = request('presta_elec');
	$Annonce->LAT = request('lat');
	$Annonce->LON = request('lon');
	$Annonce->DATE_DIANC = request('date_dianc');
	$Annonce->PRESTA_DIANC = request('presta_dianc');
	$Annonce->TAE = request('tae');
	$Annonce->PRIX_CONF = request('prix_conf');
	$Annonce->MOTS_CLES = request('mot_cle');
	$Annonce->CONTACT = request('contact');
	$Annonce->LANGUES = request('langue');
	$Annonce->AGENCE = request('agence');
	$Annonce->COPROPRIETE = request('copropriete');
	$Annonce->NB_LOTS_COPRO = request('nb_lots_copro');
	$Annonce->MONTANT_QUOTEPART = request('montant_quotepart');
	$Annonce->PROCEDURE_SYND = request('procedure_synd');
	$Annonce->PROCEDURE_DETAIL = request('procedure_detail');
	$Annonce->id = 1;
		
	$Annonce->save();
	
	return view('AjoutReussi');
});

Route::get('/AjoutAnnonceType3', function () {
    return view('Ajout/AjoutBienType3');
})->middleware('auth');
Route::post('/AjoutAnnonceType3', function () {
	$previous = new App\annonce;
	$previous->NO_ASP = request('noASP');
	$previous->id = 1;
	
	$previous->save();
	
	$Annonce = new App\annoncestype3;
	$Annonce->NO_ASP = request('noASP');
	$Annonce->CODE_SOCIETE = request('code_societe');
	$Annonce->CODE_SITE = request('code_site');
	$Annonce->NO_DOSSIER = request('no_dossier');
	$Annonce->NO_RUE = request('no_rue');
	$Annonce->TYPE_RUE = request('type_rue');
	$Annonce->NO_MANDAT = request('no_mandat');
	$Annonce->TYPE_MANDAT = request('type_mandat');
	$Annonce->ADR = request('adr');
	$Annonce->SUITE_ADR = request('suite_adr');
	$Annonce->CP = request('cp');
	$Annonce->VILLE = request('ville');
	$Annonce->PAYS = request('pays');
	$Annonce->QUARTIER = request('quartier');
	$Annonce->RESIDENCE = request('residence');
	$Annonce->TRANSPORT = request('transport');
	$Annonce->PROXIMITE = request('proximite');
	$Annonce->SITUATION = request('situation');
	$Annonce->CP_WEB = request('cp_web');
	$Annonce->VILLE_WEB = request('ville_web');
	$Annonce->MODE_CHAUFF = request('mode_chauff');
	$Annonce->REG_FISC = request('reg_fisc');
	$Annonce->FORM_JUR = request('forme_jur');
	$Annonce->CAT = request('cat');
	$Annonce->URL_VIDEO = request('url_video');
	$Annonce->ASPECT = request('aspect');
	$Annonce->PV = request('pv');
	$Annonce->HONO = request('hono');
	$Annonce->TX_HONO = request('tx_hono');
	$Annonce->TAXE_FONC = request('taxe_fonc');
	$Annonce->LONG_FAC = request('long_fac');
	$Annonce->COS = request('cos');
	$Annonce->TYPE_BIEN = request('type_bien');
	$Annonce->ETAT_AVANC = request('etat_avanc');
	$Annonce->CC = request('cc');
	$Annonce->VIAB = request('viab');
	$Annonce->GAZ = request('gaz');
	$Annonce->ASS = request('ass');
	$Annonce->ALIGN = request('align');
	$Annonce->CLOT = request('clot');
	$Annonce->DATE_CREATION = request('date_cration');
	$Annonce->DATE_MODIF = request('date_modif');
	$Annonce->DATE_MAND = request('date_mand');
	$Annonce->DATE_FIN_MAND = request('date_fin_mand');
	$Annonce->DATE_CERTIF_URB = request('date_certif_urb');
	$Annonce->DATE_CERTIF_AM = request('date_certif_am');
	$Annonce->TXT_INTERNET = request('txt_internet');
	$Annonce->TXT_PRESSE = request('txt_presse');
	$Annonce->PROF = request('prof');
	$Annonce->HAUT_MAX = request('haut_max');
	$Annonce->SURF_PLANCH = request('surf_planch');
	$Annonce->NO_PRIV = request('no_priv');
	$Annonce->SECTEUR = request('secteur');
	$Annonce->CRIT_COMP = request('crit_com');
	$Annonce->DATE_INIT_PH = request('date_init_ph');
	$Annonce->NOM_CONTACT = request('nom_contact');
	$Annonce->TEL_CONTACT = request('tel_contact');
	$Annonce->DIST = request('dist');
	$Annonce->CH_HONO = request('ch_hono');
	$Annonce->CPG_LAF = request('cpg_laf');
	$Annonce->LAT = request('lat');
	$Annonce->LON = request('lon');
	$Annonce->PRIX_CONF = request('prix_conf');
	$Annonce->MOTS_CLES = request('mot_cle');
	$Annonce->CONTACT = request('contact');
	$Annonce->AGENCE = request('agence');
	$Annonce->LANGUES = request('langue');
	$Annonce->COPROPRIETE = request('copropriete');
	$Annonce->NB_LOTS_COPRO = request('nb_lots_copro');
	$Annonce->MONTANT_QUOTEPART = request('montant_quotepart');
	$Annonce->PROCEDURE_SYND = request('procedure_synd');
	$Annonce->PROCEDURE_DETAIL = request('procedure_detail');
	$Annonce->id = 1;
		
	$Annonce->save();
	
	return view('AjoutReussi');
});

Route::get('/AjoutAnnonceType4', function () {
    return view('Ajout/AjoutBienType4');
})->middleware('auth');
Route::post('/AjoutAnnonceType4', function () {
	$previous = new App\annonce;
	$previous->NO_ASP = request('noASP');
	$previous->id = 1;
	
	$previous->save();
	
	$Annonce = new App\annoncestype4;
	$Annonce->NO_ASP = request('noASP');
	$Annonce->CODE_SOCIETE = request('code_societe');
	$Annonce->CODE_SITE = request('code_site');
	$Annonce->NO_DOSSIER = request('no_dossier');
	$Annonce->NO_RUE = request('no_rue');
	$Annonce->TYPE_RUE = request('type_rue');
	$Annonce->NO_MANDAT = request('no_mandat');
	$Annonce->TYPE_MANDAT = request('type_mandat');
	$Annonce->ADR = request('adr');
	$Annonce->SUITE_ADR = request('suite_adr');
	$Annonce->CP = request('cp');
	$Annonce->VILLE = request('ville');
	$Annonce->PAYS = request('pays');
	$Annonce->QUARTIER = request('quartier');
	$Annonce->RESIDENCE = request('residence');
	$Annonce->TRANSPORT = request('transport');
	$Annonce->PROXIMITE = request('proximite');
	$Annonce->SITUATION = request('situation');
	$Annonce->CP_WEB = request('cp_web');
	$Annonce->VILLE_WEB = request('ville_web');
	$Annonce->MODE_CHAUFF = request('mode_chauff');
	$Annonce->REG_FISC = request('reg_fisc');
	$Annonce->FORM_JUR = request('forme_jur');
	$Annonce->CAT = request('cat');
	$Annonce->TYPE_CHAUFF = request('type_chauff');
	$Annonce->NAT_CHAUFF = request('nat_chauff');
	$Annonce->STANDING = request('standing');
	$Annonce->TYPE_CONS = request('type_cons');
	$Annonce->COUV = request('couv');
	$Annonce->FACADE = request('facade');
	$Annonce->ETAT_EXT = request('etat_ext');
	$Annonce->EAU_CHAUDE = request('eau_chaude');
	$Annonce->DIGICODE = request('digicode');
	$Annonce->CERTIF_CAR = request('certif_car');
	$Annonce->CERTIF_AM = request('certif_am');
	$Annonce->CERTIF_PB = request('certif_pb');
	$Annonce->CERTIF_TER = request('certif_ter');
	$Annonce->URL_VIDEO = request('url_video');
	$Annonce->PV = request('pv');
	$Annonce->HONO = request('hono');
	$Annonce->TX_HONO = request('tx_hono');
	$Annonce->TRAVAUX = request('travaux');
	$Annonce->TAXE_FONC = request('taxe_fonc');
	$Annonce->CHARGES = request('charges');
	$Annonce->LOYER = request('loyer');
	$Annonce->SURF_TOT = request('surf_tot');
	$Annonce->SURF_CAD = request('surf_cad');
	$Annonce->TYPE_BIEN = request('type_bien');
	$Annonce->ETAT_AVANC = request('etat_avanc');
	$Annonce->NB_LOT = request('nb_lot');
	$Annonce->NB_ETAGE = request('nb_etage');
	$Annonce->NB_PARK_INT = request('nb_park_int');
	$Annonce->NB_PARK_EXT = request('nb_park_ext');
	$Annonce->NB_BOX = request('nb_box');
	$Annonce->ANNEE_CONS = request('annee_cons');
	$Annonce->DISPO = request('dispo');
	$Annonce->NB_CAVE = request('nb_cave');
	$Annonce->ASCE = request('asce');
	$Annonce->HAND = request('hand');
	$Annonce->INTERPHONE = request('interphone');
	$Annonce->HAS_DIGICODE = request('digicode');
	$Annonce->CC = request('cc');
	$Annonce->PISCINE = request('piscine');
	$Annonce->DATE_CREATION = request('date_cration');
	$Annonce->DATE_MODIF = request('date_modif');
	$Annonce->DATE_MAND = request('date_mand');
	$Annonce->DATE_FIN_MAND = request('date_fin_mand');
	$Annonce->DATE_LIBER = request('date_liber');
	$Annonce->DATE_MODIF_PRIX = request('date_modif_prix');
	$Annonce->DATE_CAR = request('date_car');
	$Annonce->DATE_AM = request('date_am');
	$Annonce->DATE_PB = request('date_pb');
	$Annonce->DATE_TER = request('date_ter');
	$Annonce->TXT_INTERNET = request('txt_internet');
	$Annonce->TXT_PRESSE = request('txt_presse');
	$Annonce->ETAT_INT = request('etat_int');
	$Annonce->NB_NIV = request('nb_niv');
	$Annonce->SURF_HABITATION = request('surf_habitation');
	$Annonce->SURF_COMM = request('surf_comm');
	$Annonce->SURF_HO = request('surf_ho');
	$Annonce->REV_B_AN = request('rev_b_an');
	$Annonce->REV_HAB = request('rev_hab');
	$Annonce->REV_COMM = request('rev_comm');
	$Annonce->GARDIEN = request('gardien');
	$Annonce->PREST = request('prestige');
	$Annonce->IMM_COL = request('imm_col');
	$Annonce->IMM_INDEP = request('imm_indep');
	$Annonce->NO_PRIV = request('no_priv');
	$Annonce->SECTEUR = request('secteur');
	$Annonce->CRIT_COMP = request('crit_com');
	$Annonce->SURF_JARD = request('surf_jar');
	$Annonce->SURF_UTILE = request('surf_utile');
	$Annonce->SURF_DEV = request('surf_dev');
	$Annonce->DATE_INIT_PH = request('date_init_ph');
	$Annonce->DATE_DPE = request('date_dpe');
	$Annonce->DATE_ERNT = request('date_ernt');
	$Annonce->DATE_GAZ = request('date_gaz');
	$Annonce->PRESTA_DPE = request('presta_dpe');
	$Annonce->PRESTA_ERNT = request('presta_erng');
	$Annonce->PRESTA_GAZ = request('presta_gaz');
	$Annonce->NOM_CONTACT = request('nom_contact');
	$Annonce->TEL_CONTACT = request('tel_contact');
	$Annonce->DIST = request('dist');
	$Annonce->CH_HONO = request('ch_hono');
	$Annonce->DPE = request('dpe');
	$Annonce->GES = request('ges');
	$Annonce->VAL_DPE = request('val_dpe');
	$Annonce->VAL_GES = request('val_ges');
	$Annonce->CPG_LAF = request('cpg_laf');
	$Annonce->LAT = request('lat');
	$Annonce->LON = request('lon');
	$Annonce->DATE_DIANC = request('date_dianc');
	$Annonce->PRESTA_DIANC = request('presta_dianc');
	$Annonce->TAE = request('tae');
	$Annonce->PRIX_CONF = request('prix_conf');
	$Annonce->MOTS_CLES = request('mot_cle');
	$Annonce->CONTACT = request('contact');
	$Annonce->AGENCE = request('agence');
	$Annonce->LANGUES = request('langue');
	$Annonce->COPROPRIETE = request('copropriete');
	$Annonce->NB_LOTS_COPRO = request('nb_lots_copro');
	$Annonce->MONTANT_QUOTEPART = request('montant_quotepart');
	$Annonce->PROCEDURE_SYND = request('procedure_synd');
	$Annonce->PROCEDURE_DETAIL = request('procedure_detail');
	$Annonce->id = 1;
		
	$Annonce->save();
	
	return view('AjoutReussi');
});

Route::get('/AjoutAnnonceType5', function () {
    return view('Ajout/AjoutBienType5');
})->middleware('auth');
Route::post('/AjoutAnnonceType5', function () {
	$previous = new App\annonce;
	$previous->NO_ASP = request('noASP');
	$previous->id = 1;
	
	$previous->save();
	
	$Annonce = new App\annoncestype5;
	$Annonce->NO_ASP = request('noASP');
	$Annonce->CODE_SOCIETE = request('code_societe');
	$Annonce->CODE_SITE = request('code_site');
	$Annonce->NO_DOSSIER = request('no_dossier');
	$Annonce->NO_RUE = request('no_rue');
	$Annonce->TYPE_RUE = request('type_rue');
	$Annonce->NO_MANDAT = request('no_mandat');
	$Annonce->TYPE_MANDAT = request('type_mandat');
	$Annonce->ADR = request('adr');
	$Annonce->SUITE_ADR = request('suite_adr');
	$Annonce->CP = request('cp');
	$Annonce->VILLE = request('ville');
	$Annonce->PAYS = request('pays');
	$Annonce->CP_WEB = request('cp_web');
	$Annonce->VILLE_WEB = request('ville_web');
	$Annonce->MODE_CHAUFF = request('mode_chauff');
	$Annonce->REG_FISC = request('reg_fisc');
	$Annonce->FORM_JUR = request('forme_jur');
	$Annonce->CAT = request('cat');
	$Annonce->TYPE_CHAUFF = request('type_chauff');
	$Annonce->NAT_CHAUFF = request('nat_chauff');
	$Annonce->TYPE_BAIL_OCC = request('type_bail_occ');
	$Annonce->STANDING = request('standing');
	$Annonce->TYPE_CONS = request('type_cons');
	$Annonce->COUV = request('couv');
	$Annonce->FACADE = request('facade');
	$Annonce->ETAT_EXT = request('etat_ext');
	$Annonce->DIGICODE = request('digicode');
	$Annonce->URL_VIDEO = request('url_video');
	$Annonce->PV = request('pv');
	$Annonce->HONO = request('hono');
	$Annonce->TX_HONO = request('tx_hono');
	$Annonce->TAXE_BUR = request('taxe_bur');
	$Annonce->TAXE_FONC = request('taxe_fonc');
	$Annonce->SURF_TERR = request('surf_terr');
	$Annonce->CHARGES_ANN = request('charges_ann');
	$Annonce->LOYER_ANN = request('loyer_ann');
	$Annonce->SURF_TOT = request('surf_tot');
	$Annonce->SURF_MINI = request('surf_mini');
	$Annonce->TYPE_BIEN = request('type_bien');
	$Annonce->ETAT_AVANC = request('etat_avanc');
	$Annonce->NB_ETAGE = request('nb_etage');
	$Annonce->NB_PARK_INT = request('nb_park_int');
	$Annonce->NB_PARK_EXT = request('nb_park_ext');
	$Annonce->NB_BOX = request('nb_box');
	$Annonce->ANNEE_CONS = request('annee_cons');
	$Annonce->DISPO = request('dispo');
	$Annonce->ASCE = request('asce');
	$Annonce->HAND = request('hand');
	$Annonce->INTERPHONE = request('interphone');
	$Annonce->HAS_DIGICODE = request('has_digicode');
	$Annonce->U_BUR = request('u_bur');
	$Annonce->U_COMM = request('u_comm');
	$Annonce->U_ACT = request('u_act');
	$Annonce->U_PRO = request('u_pro');
	$Annonce->CC = request('cc');
	$Annonce->MC = request('mc');
	$Annonce->TELESURV = request('telesurv');
	$Annonce->DATE_CREATION = request('date_cration');
	$Annonce->DATE_MODIF = request('date_modif');
	$Annonce->DATE_MAND = request('date_mand');
	$Annonce->DATE_FIN_MAND = request('date_fin_mand');
	$Annonce->DATE_LIBER = request('date_liber');
	$Annonce->DATE_MODIF_PRIX = request('date_modif_prix');
	$Annonce->DATE_BAIL = request('date_bail');
	$Annonce->DATE_DISP = request('date_disp');
	$Annonce->TXT_INTERNET = request('txt_internet');
	$Annonce->TXT_PRESSE = request('txt_presse');
	$Annonce->ETAT_INT = request('etat_int');
	$Annonce->NB_NIV = request('nb_niv');
	$Annonce->LON_VIT = request('lon_vit');
	$Annonce->GARDIENNAGE = request('gardiennage');
	$Annonce->PREST = request('prestige');
	$Annonce->NON_DPE = request('non_dpe');
	$Annonce->IMM_COL = request('imm_col');
	$Annonce->IMM_INDEP = request('imm_indep');
	$Annonce->CLIM = request('clim');
	$Annonce->RIE = request('rie');
	$Annonce->NO_PRIV = request('no_priv');
	$Annonce->SECTEUR = request('secteur');
	$Annonce->CRIT_COMP = request('crit_com');
	$Annonce->DESSERTE = request('desserte');
	$Annonce->HSP = request('hsp');
	$Annonce->SURF_BUR = request('surf_bur');
	$Annonce->DATE_INIT_PH = request('date_init_ph');
	$Annonce->NOM_CONTACT = request('nom_contact');
	$Annonce->TEL_CONTACT = request('tel_contact');
	$Annonce->DIST = request('dist');
	$Annonce->CH_HONO = request('ch_hono');
	$Annonce->DPE = request('dpe');
	$Annonce->GES = request('ges');
	$Annonce->VAL_DPE = request('val_dpe');
	$Annonce->VAL_GES = request('val_ges');
	$Annonce->CPG_LAF = request('cpg_laf');
	$Annonce->LAT = request('lat');
	$Annonce->LON = request('lon');
	$Annonce->PRIX_CONF = request('prix_conf');
	$Annonce->MOTS_CLES = request('mot_cle');
	$Annonce->CONTACT = request('contact');
	$Annonce->AGENCE = request('agence');
	$Annonce->LANGUES = request('langue');
	$Annonce->COPROPRIETE = request('copropriete');
	$Annonce->NB_LOTS_COPRO = request('nb_lots_copro');
	$Annonce->MONTANT_QUOTEPART = request('montant_quotepart');
	$Annonce->PROCEDURE_SYND = request('procedure_synd');
	$Annonce->PROCEDURE_DETAIL = request('procedure_detail');
	$Annonce->id = 1;
		
	$Annonce->save();
	
	return view('AjoutReussi');
});

Route::get('/AjoutAnnonceType6', function () {
    return view('Ajout/AjoutBienType6');
})->middleware('auth');
Route::post('/AjoutAnnonceType6', function () {
	$previous = new App\annonce;
	$previous->NO_ASP = request('noASP');
	$previous->id = 1;
	
	$previous->save();
	
	$Annonce = new App\annoncestype6;
	$Annonce->NO_ASP = request('noASP');
	$Annonce->CODE_SOCIETE = request('code_societe');
	$Annonce->CODE_SITE = request('code_site');
	$Annonce->NO_DOSSIER = request('no_dossier');
	$Annonce->NO_RUE = request('no_rue');
	$Annonce->TYPE_RUE = request('type_rue');
	$Annonce->NO_MANDAT = request('no_mandat');
	$Annonce->TYPE_MANDAT = request('type_mandat');
	$Annonce->ADR = request('adr');
	$Annonce->SUITE_ADR = request('suite_adr');
	$Annonce->CP = request('cp');
	$Annonce->VILLE = request('ville');
	$Annonce->PAYS = request('pays');
	$Annonce->CP_WEB = request('cp_web');
	$Annonce->VILLE_WEB = request('ville_web');
	$Annonce->REG_FISC = request('reg_fisc');
	$Annonce->FORM_JUR = request('forme_jur');
	$Annonce->CAT = request('cat');
	$Annonce->ACTIVITE = request('activite');
	$Annonce->RAISON_SOC = request('raison_soc');
	$Annonce->ENSEIGNE = request('enseigne');
	$Annonce->URL_VIDEO = request('url_video');
	$Annonce->TYPE_BAIL = request('type_bail');
	$Annonce->DIGICODE = request('digicode');
	$Annonce->PV = request('pv');
	$Annonce->HONO = request('hono');
	$Annonce->TX_HONO = request('tx_hono');
	$Annonce->CAP_SOC = request('cap_soc');
	$Annonce->TAXE_PROF = request('taxe_prof');
	$Annonce->MAS_SAL = request('mas_sal');
	$Annonce->CHARGES = request('charges');
	$Annonce->LOYER = request('loyer');
	$Annonce->SURF_PROF = request('surf_prof');
	$Annonce->SURF_ANNEXE = request('surf_annexe');
	$Annonce->SURF_TERR = request('surf_terr');
	$Annonce->TYPE_BIEN = request('type_bien');
	$Annonce->ETAT_AVANC = request('etat_avanc');
	$Annonce->NB_PCE = request('nb_pce');
	$Annonce->NB_EMP = request('nb_emp');
	$Annonce->ANNEE1 = request('annee1');
	$Annonce->ANNEE2 = request('annee2');
	$Annonce->ANNEE3 = request('annee3');
	$Annonce->NB_PARK_INT = request('nb_park_int');
	$Annonce->NB_PARK_EXT = request('nb_park_ext');
	$Annonce->NB_BOX = request('nb_box');
	$Annonce->CC = request('cc');
	$Annonce->DATE_CREATION = request('date_cration');
	$Annonce->DATE_MODIF = request('date_modif');
	$Annonce->DATE_MAND = request('date_mand');
	$Annonce->DATE_FIN_MAND = request('date_fin_mand');
	$Annonce->DATE_CREAT_FDC = request('date_creat_fdc');
	$Annonce->DATE_MODIF_PRIX = request('date_modif_prix');
	$Annonce->TXT_INTERNET = request('txt_internet');
	$Annonce->TXT_PRESSE = request('txt_presse');
	$Annonce->LON_VIT = request('lon_vit');
	$Annonce->SURF_LOG = request('surf_log');
	$Annonce->LON_VIT = request('lon_vit');
	$Annonce->TAUX_TAXE_PROF = request('taux_taxe_prof');
	$Annonce->CA_AN1 = request('ca_an1');
	$Annonce->CA_AN2 = request('ca_an2');
	$Annonce->CA_AN3 = request('ca_an3');
	$Annonce->BIC_AN1 = request('bic_an1');
	$Annonce->BIC_AN2 = request('bic_an2');
	$Annonce->BIC_AN3 = request('bic_an3');
	$Annonce->SAL_EXPLOIT = request('sal_exploit');
	$Annonce->VAL_STOCK = request('val_stock');
	$Annonce->CASH_FLOW = request('cash_flow');
	$Annonce->NON_DPE = request('non_dpe');
	$Annonce->NO_PRIV = request('no_priv');
	$Annonce->SECTEUR = request('secteur');
	$Annonce->CRIT_COMP = request('crit_com');
	$Annonce->DATE_FIN_BAIL = request('date_fin_bail');
	$Annonce->DATE_INIT_PH = request('date_init_ph');
	$Annonce->NOM_CONTACT = request('nom_contact');
	$Annonce->TEL_CONTACT = request('tel_contact');
	$Annonce->DIST = request('dist');
	$Annonce->CH_HONO = request('ch_hono');
	$Annonce->DPE = request('dpe');
	$Annonce->GES = request('ges');
	$Annonce->VAL_DPE = request('val_dpe');
	$Annonce->VAL_GES = request('val_ges');
	$Annonce->CPG_LAF = request('cpg_laf');
	$Annonce->LAT = request('lat');
	$Annonce->LON = request('lon');
	$Annonce->PRIX_CONF = request('prix_conf');
	$Annonce->MOTS_CLES = request('mot_cle');
	$Annonce->CONTACT = request('contact');
	$Annonce->AGENCE = request('agence');
	$Annonce->LANGUES = request('langue');
	$Annonce->COPROPRIETE = request('copropriete');
	$Annonce->NB_LOTS_COPRO = request('nb_lots_copro');
	$Annonce->MONTANT_QUOTEPART = request('montant_quotepart');
	$Annonce->PROCEDURE_SYND = request('procedure_synd');
	$Annonce->PROCEDURE_DETAIL = request('procedure_detail');
	$Annonce->id = 1;
		
	$Annonce->save();
	
	return view('AjoutReussi');
});

Route::get('/AjoutAnnonceType7', function () {
    return view('Ajout/AjoutBienType7');
})->middleware('auth');
Route::post('/AjoutAnnonceType7', function () {
	$previous = new App\annonce;
	$previous->NO_ASP = request('noASP');
	$previous->id = 1;
	
	$previous->save();
	
	$Annonce = new App\annoncestype7;
	$Annonce->NO_ASP = request('noASP');
	$Annonce->CODE_SOCIETE = request('code_societe');
	$Annonce->CODE_SITE = request('code_site');
	$Annonce->NO_DOSSIER = request('no_dossier');
	$Annonce->NO_RUE = request('no_rue');
	$Annonce->TYPE_RUE = request('type_rue');
	$Annonce->NO_MANDAT = request('no_mandat');
	$Annonce->TYPE_MANDAT = request('type_mandat');
	$Annonce->ADR = request('adr');
	$Annonce->SUITE_ADR = request('suite_adr');
	$Annonce->CP = request('cp');
	$Annonce->VILLE = request('ville');
	$Annonce->PAYS = request('pays');
	$Annonce->QUARTIER = request('quartier');
	$Annonce->NOM_PARK = request('nom_park');
	$Annonce->TRANSPORT = request('transport');
	$Annonce->PROXIMITE = request('proximite');
	$Annonce->SITUATION = request('situation');
	$Annonce->CP_WEB = request('cp_web');
	$Annonce->VILLE_WEB = request('ville_web');
	$Annonce->REG_FISC = request('reg_fisc');
	$Annonce->FORM_JUR = request('forme_jur');
	$Annonce->CAT = request('cat');
	$Annonce->URL_VIDEO = request('url_video');
	$Annonce->TYPE_CONS = request('type_cons');
	$Annonce->ETAT_GEN = request('etat_gen');
	$Annonce->DIGICODE = request('digicode');
	$Annonce->PV = request('pv');
	$Annonce->HONO = request('hono');
	$Annonce->TX_HONO = request('tx_hono');
	$Annonce->TAXE_FONC = request('taxe_fonc');
	$Annonce->TAXE_PROF = request('taxe_prof');
	$Annonce->LOYER = request('loyer');
	$Annonce->SURF = request('surf');
	$Annonce->TYPE_BIEN = request('type_bien');
	$Annonce->ETAT_AVANC = request('etat_avanc');
	$Annonce->NIVEAU = request('niveau');
	$Annonce->NB_NIV = request('nb_niv');
	$Annonce->TYPE_STAT = request('type_stat');
	$Annonce->TYPE_ACCES = request('type_acces');
	$Annonce->ACCES_PIETON = request('acces_pieton');
	$Annonce->NB_VOITURE = request('nb_voiture');
	$Annonce->ANNEE_CONS = request('annee_cons');
	$Annonce->ASCE = request('asce');
	$Annonce->HAND = request('hand');
	$Annonce->BIP = request('bip');
	$Annonce->CARTE_MAGN = request('carte_magn');
	$Annonce->VIDEO_SURV = request('video_surv');
	$Annonce->CC = request('cc');
	$Annonce->DATE_CREATION = request('date_cration');
	$Annonce->DATE_MODIF = request('date_modif');
	$Annonce->DATE_MAND = request('date_mand');
	$Annonce->DATE_FIN_MAND = request('date_fin_mand');
	$Annonce->DATE_LIBER = request('date_liber');
	$Annonce->DATE_MODIF_PRIX = request('date_modif_prix');
	$Annonce->TXT_INTERNET = request('txt_internet');
	$Annonce->TXT_PRESSE = request('txt_presse');
	$Annonce->LONGUEUR = request('longeur');
	$Annonce->LARGEUR = request('largeur');
	$Annonce->HAUTEUR = request('hauteur');
	$Annonce->GARDIENNAGE = request('gardiennage');
	$Annonce->NUM_EMP = request('num_emp');
	$Annonce->NO_PRIV = request('no_priv');
	$Annonce->SECTEUR = request('secteur');
	$Annonce->CRIT_COMP = request('crit_com');
	$Annonce->DATE_INIT_PH = request('date_init_ph');
	$Annonce->NOM_CONTACT = request('nom_contact');
	$Annonce->TEL_CONTACT = request('tel_contact');
	$Annonce->DIST = request('dist');
	$Annonce->CH_HONO = request('ch_hono');
	$Annonce->CPG_LAF = request('cpg_laf');
	$Annonce->LAT = request('lat');
	$Annonce->LON = request('lon');
	$Annonce->PRIX_CONF = request('prix_conf');
	$Annonce->MOTS_CLES = request('mot_cle');
	$Annonce->CONTACT = request('contact');
	$Annonce->AGENCE = request('agence');
	$Annonce->LANGUES = request('langue');
	$Annonce->COPROPRIETE = request('copropriete');
	$Annonce->NB_LOTS_COPRO = request('nb_lots_copro');
	$Annonce->MONTANT_QUOTEPART = request('montant_quotepart');
	$Annonce->PROCEDURE_SYND = request('procedure_synd');
	$Annonce->PROCEDURE_DETAIL = request('procedure_detail');
	$Annonce->id = 1;
		
	$Annonce->save();
	
	return view('AjoutReussi');
});

Route::get('/AjoutAnnonceType11', function () {
    return view('Ajout/AjoutBienType11');
})->middleware('auth');
Route::post('/AjoutAnnonceType11', function () {
	$previous = new App\annonce;
	$previous->NO_ASP = request('noASP');
	$previous->id = 1;
	
	$previous->save();
	
	$Annonce = new App\annoncestype11;
	$Annonce->NO_ASP = request('noASP');
	$Annonce->CODE_SOCIETE = request('code_societe');
	$Annonce->CODE_SITE = request('code_site');
	$Annonce->NO_DOSSIER = request('no_dossier');
	$Annonce->NO_RUE = request('no_rue');
	$Annonce->TYPE_RUE = request('type_rue');
	$Annonce->NO_MANDAT = request('no_mandat');
	$Annonce->TYPE_MANDAT = request('type_mandat');
	$Annonce->ADR = request('adr');
	$Annonce->SUITE_ADR = request('suite_adr');
	$Annonce->CP = request('cp');
	$Annonce->VILLE = request('ville');
	$Annonce->PAYS = request('pays');
	$Annonce->QUARTIER = request('quartier');
	$Annonce->RESIDENCE = request('residence');
	$Annonce->TRANSPORT = request('transport');
	$Annonce->PROXIMITE = request('proximite');
	$Annonce->SITUATION = request('situation');
	$Annonce->CP_WEB = request('cp_web');
	$Annonce->VILLE_WEB = request('ville_web');
	$Annonce->MODE_CHAUFF = request('mode_chauff');
	$Annonce->CAT = request('cat');
	$Annonce->TYPE_CHAUFF = request('type_chauff');
	$Annonce->NAT_CHAUFF = request('nat_chauff');
	$Annonce->TYPE_CUIS = request('type_cuis');
	$Annonce->STANDING = request('standing');
	$Annonce->COUV = request('couv');
	$Annonce->FACADE = request('facade');
	$Annonce->ETAT_EXT = request('etat_ext');
	$Annonce->EAU_CHAUDE = request('eau_chaude');
	$Annonce->DIGICODE = request('digicode');
	$Annonce->CE = request('ce');
	$Annonce->URL_VIDEO = request('url_video');
	$Annonce->TYPE_CONS = request('type_cons');
	$Annonce->TAXE_HAB = request('taxe_hab');
	$Annonce->REF_ADB = request('ref_adb');
	$Annonce->REGIME_BAIL = request('regime_bail');
	$Annonce->TYPE_CAUTION = request('type_caution');
	$Annonce->LOYER_HC = request('loyer_hc');
	$Annonce->LOYER_PARK = request('loyer_park');
	$Annonce->PROV_CH = request('prov_ch');
	$Annonce->PROV_CHAUFF = request('prov_chauff');
	$Annonce->FRAIS_DIV = request('frais_div');
	$Annonce->LOYER_CC = request('loyer_cc');
	$Annonce->TRAVAUX = request('travaux');
	$Annonce->DEP_G = request('dep_g');
	$Annonce->HRAL = request('hral');
	$Annonce->SURF_HAB = request('surf_hab');
	$Annonce->HRAP = request('hrap');
	$Annonce->SURF_JAR = request('surf_jar');
	$Annonce->TYPE_BIEN = request('type_bien');
	$Annonce->ETAT_AVANC = request('etat_avanc');
	$Annonce->NB_PCE = request('nb_pce');
	$Annonce->NB_CHB = request('nb_chb');
	$Annonce->ETAGE = request('etage');
	$Annonce->NB_ETAGE = request('nb_etage');
	$Annonce->NB_WC = request('nb_wc');
	$Annonce->NB_SDB = request('nb_sdb');
	$Annonce->NB_SE = request('nb_se');
	$Annonce->NB_PARK_INT = request('nb_park_int');
	$Annonce->NB_PARK_EXT = request('nb_park_ext');
	$Annonce->NB_BOX = request('nb_box');
	$Annonce->ANNEE_CONS = request('annee_cons');
	$Annonce->NB_CAVE = request('nb_cave');
	$Annonce->NB_BALCON = request('nb_balcon');
	$Annonce->ASCE = request('asce');
	$Annonce->HAND = request('hand');
	$Annonce->INTERPHONE = request('interphone');
	$Annonce->HAS_DIGICODE = request('has_digicode');
	$Annonce->CAUTION = request('caution');
	$Annonce->OCC_PROP = request('occ_prop');
	$Annonce->CC = request('cc');
	$Annonce->PISCINE = request('piscine');
	$Annonce->MEUBLE = request('meuble');
	$Annonce->DATE_CREATION = request('date_cration');
	$Annonce->DATE_MODIF = request('date_modif');
	$Annonce->DATE_MAND = request('date_mand');
	$Annonce->DATE_FIN_MAND = request('date_fin_mand');
	$Annonce->DATE_LIBER = request('date_liber');
	$Annonce->DATE_DISP = request('date_disp');
	$Annonce->DATE_FIN_BAIL = request('date_fin_bail');
	$Annonce->TXT_INTERNET = request('txt_internet');
	$Annonce->ETAT_INT = request('etat_int');
	$Annonce->NB_TERR = request('nb_terr');
	$Annonce->NB_NIV = request('nb_niv');
	$Annonce->DUREE_BAIL = request('duree_bail');
	$Annonce->SURF_SEJ = request('surf_sej');
	$Annonce->NON_DPE = request('non_dpe');
	$Annonce->HONO_LOC = request('hono_loc');
	$Annonce->SECTEUR = request('secteur');
	$Annonce->CRIT_COMP = request('crit_com');
	$Annonce->MANDAT_ALG = request('mandat_alg');
	$Annonce->DATE_INIT_PH = request('date_init_ph');
	$Annonce->DATE_DPE = request('date_dpe');
	$Annonce->DATE_ERNT = request('date_ernt');
	$Annonce->DATE_PB = request('date_pb');
	$Annonce->PRESTA_DPE = request('presta_dpe');
	$Annonce->PRESTA_ERNT = request('presta_erng');
	$Annonce->PRESTA_PB = request('presta_pb');
	$Annonce->NOM_CONTACT = request('nom_contact');
	$Annonce->TEL_CONTACT = request('tel_contact');
	$Annonce->DIST = request('dist');
	$Annonce->TOTAL_HONO_TTC = request('total_hono_ttc');
	$Annonce->DPE = request('dpe');
	$Annonce->GES = request('ges');
	$Annonce->VAL_DPE = request('val_dpe');
	$Annonce->VAL_GES = request('val_ges');
	$Annonce->TOTAL_HONO_HT = request('total_hono_ht');
	$Annonce->ALI = request('ali');
	$Annonce->LAT = request('lat');
	$Annonce->LON = request('lon');
	$Annonce->PRIX_CONF = request('prix_conf');
	$Annonce->MOTS_CLES = request('mot_cle');
	$Annonce->CONTACT = request('contact');
	$Annonce->AGENCE = request('agence');
	$Annonce->LANGUES = request('langue');
	$Annonce->INDICE_NATURE_CHARGE = request('indice_nature_charge');
	$Annonce->COMPLEMENT_LOYER = request('complement_loyer');
	$Annonce->INDICE_REGLEMENT_LOYER = request('indice_complement_loyer');
	$Annonce->INDICE_REGLEMENT_CHARGES = request('indice_reglement_charge');
	$Annonce->HONO_ETAT_LIEU_LOC = request('hono_etat_lieu_loc');
	$Annonce->id = 1;
		
	$Annonce->save();
	
	return view('AjoutReussi');
});

Route::get('/AjoutAnnonceType12', function () {
    return view('Ajout/AjoutBienType12');
})->middleware('auth');
Route::post('/AjoutAnnonceType12', function () {
	$previous = new App\annonce;
	$previous->NO_ASP = request('noASP');
	$previous->id = 1;
	
	$previous->save();
	
	$Annonce = new App\annoncestype12;
	$Annonce->NO_ASP = request('noASP');
	$Annonce->CODE_SOCIETE = request('code_societe');
	$Annonce->CODE_SITE = request('code_site');
	$Annonce->NO_DOSSIER = request('no_dossier');
	$Annonce->NO_RUE = request('no_rue');
	$Annonce->TYPE_RUE = request('type_rue');
	$Annonce->NO_MANDAT = request('no_mandat');
	$Annonce->TYPE_MANDAT = request('type_mandat');
	$Annonce->ADR = request('adr');
	$Annonce->SUITE_ADR = request('suite_adr');
	$Annonce->CP = request('cp');
	$Annonce->VILLE = request('ville');
	$Annonce->PAYS = request('pays');
	$Annonce->QUARTIER = request('quartier');
	$Annonce->RESIDENCE = request('residence');
	$Annonce->TRANSPORT = request('transport');
	$Annonce->PROXIMITE = request('proximite');
	$Annonce->SITUATION = request('situation');
	$Annonce->CP_WEB = request('cp_web');
	$Annonce->VILLE_WEB = request('ville_web');
	$Annonce->MODE_CHAUFF = request('mode_chauff');
	$Annonce->CAT = request('cat');
	$Annonce->TYPE_CHAUFF = request('type_chauff');
	$Annonce->NAT_CHAUFF = request('nat_chauff');
	$Annonce->TYPE_CUIS = request('type_cuis');
	$Annonce->STANDING = request('standing');
	$Annonce->COUV = request('couv');
	$Annonce->FACADE = request('facade');
	$Annonce->ETAT_EXT = request('etat_ext');
	$Annonce->EAU_CHAUDE = request('eau_chaude');
	$Annonce->DIGICODE = request('digicode');
	$Annonce->URL_VIDEO = request('url_video');
	$Annonce->TYPE_CONS = request('type_cons');
	$Annonce->TAXE_HAB = request('taxe_hab');
	$Annonce->REF_ADB = request('ref_adb');
	$Annonce->REGIME_BAIL = request('regime_bail');
	$Annonce->TYPE_CAUTION = request('type_caution');
	$Annonce->LOYER_HC = request('loyer_hc');
	$Annonce->LOYER_PARK = request('loyer_park');
	$Annonce->PROV_CH = request('prov_ch');
	$Annonce->PROV_CHAUFF = request('prov_chauff');
	$Annonce->FRAIS_DIV = request('frais_div');
	$Annonce->LOYER_CC = request('loyer_cc');
	$Annonce->TRAVAUX = request('travaux');
	$Annonce->DEP_G = request('dep_g');
	$Annonce->HRAL = request('hral');
	$Annonce->SURF_HAB = request('surf_hab');
	$Annonce->HRAP = request('hrap');
	$Annonce->SURF_TERR = request('surf_terr');
	$Annonce->TYPE_BIEN = request('type_bien');
	$Annonce->ETAT_AVANC = request('etat_avanc');
	$Annonce->NB_PCE = request('nb_pce');
	$Annonce->NB_CHB = request('nb_chb');
	$Annonce->NB_MUR_MIT = request('nb_mur_mit');
	$Annonce->NB_WC = request('nb_wc');
	$Annonce->NB_SDB = request('nb_sdb');
	$Annonce->NB_SE = request('nb_se');
	$Annonce->NB_PARK_INT = request('nb_park_int');
	$Annonce->NB_PARK_EXT = request('nb_park_ext');
	$Annonce->NB_GAR = request('nb_gar');
	$Annonce->ANNEE_CONS = request('annee_cons');
	$Annonce->NB_CAVE = request('nb_cave');
	$Annonce->NB_BALCON = request('nb_balcon');
	$Annonce->LOTISS = request('lotiss');
	$Annonce->HAND = request('hand');
	$Annonce->INTERPHONE = request('interphone');
	$Annonce->ALARME = request('alarme');
	$Annonce->CAUTION = request('caution');
	$Annonce->OCC_PROP = request('occ_prop');
	$Annonce->CC = request('cc');
	$Annonce->PISCINE = request('piscine');
	$Annonce->MEUBLE = request('meuble');
	$Annonce->DATE_CREATION = request('date_cration');
	$Annonce->DATE_MODIF = request('date_modif');
	$Annonce->DATE_MAND = request('date_mand');
	$Annonce->DATE_FIN_MAND = request('date_fin_mand');
	$Annonce->DATE_LIBER = request('date_liber');
	$Annonce->DATE_DISP = request('date_disp');
	$Annonce->DATE_FIN_BAIL = request('date_fin_bail');
	$Annonce->TXT_INTERNET = request('txt_internet');
	$Annonce->ETAT_INT = request('etat_int');
	$Annonce->NB_TERR = request('nb_terr');
	$Annonce->NB_NIV = request('nb_niv');
	$Annonce->DUREE_BAIL = request('duree_bail');
	$Annonce->SURF_SEJ = request('surf_sej');
	$Annonce->SURF_DEP = request('surf_dep');
	$Annonce->SS = request('ss');
	$Annonce->NON_DPE = request('non_dpe');
	$Annonce->HONO_LOC = request('hono_loc');
	$Annonce->SECTEUR = request('secteur');
	$Annonce->CRIT_COMP = request('crit_com');
	$Annonce->SURF_JARD = request('surf_jard');
	$Annonce->MANDAT_ALG = request('mandat_alg');
	$Annonce->DATE_INIT_PH = request('date_init_ph');
	$Annonce->DATE_DPE = request('date_dpe');
	$Annonce->DATE_ERNT = request('date_ernt');
	$Annonce->DATE_PB = request('date_pb');
	$Annonce->PRESTA_DPE = request('presta_dpe');
	$Annonce->PRESTA_ERNT = request('presta_erng');
	$Annonce->PRESTA_PB = request('presta_pb');
	$Annonce->NOM_CONTACT = request('nom_contact');
	$Annonce->TEL_CONTACT = request('tel_contact');
	$Annonce->DIST = request('dist');
	$Annonce->TOTAL_HONO_TTC = request('total_hono_ttc');
	$Annonce->DPE = request('dpe');
	$Annonce->GES = request('ges');
	$Annonce->VAL_DPE = request('val_dpe');
	$Annonce->VAL_GES = request('val_ges');
	$Annonce->TOTAL_HONO_HT = request('total_hono_ht');
	$Annonce->ALI = request('ali');
	$Annonce->LAT = request('lat');
	$Annonce->LON = request('lon');
	$Annonce->PRIX_CONF = request('prix_conf');
	$Annonce->MOTS_CLES = request('mot_cle');
	$Annonce->CONTACT = request('contact');
	$Annonce->AGENCE = request('agence');
	$Annonce->LANGUES = request('langue');
	$Annonce->INDICE_NATURE_CHARGE = request('indice_nature_charge');
	$Annonce->COMPLEMENT_LOYER = request('complement_loyer');
	$Annonce->INDICE_REGLEMENT_LOYER = request('indice_complement_loyer');
	$Annonce->INDICE_REGLEMENT_CHARGES = request('indice_reglement_charge');
	$Annonce->HONO_ETAT_LIEU_LOC = request('hono_etat_lieu_loc');
	$Annonce->id = 1;
		
	$Annonce->save();
	
	return view('AjoutReussi');
});

Route::get('/AjoutAnnonceType13', function () {
    return view('Ajout/AjoutBienType13');
})->middleware('auth');
Route::post('/AjoutAnnonceType13', function () {
	$previous = new App\annonce;
	$previous->NO_ASP = request('noASP');
	$previous->id = 1;
	
	$previous->save();
	
	$Annonce = new App\annoncestype13;
	$Annonce->NO_ASP = request('noASP');
	$Annonce->CODE_SOCIETE = request('code_societe');
	$Annonce->CODE_SITE = request('code_site');
	$Annonce->NO_DOSSIER = request('no_dossier');
	$Annonce->NO_RUE = request('no_rue');
	$Annonce->TYPE_RUE = request('type_rue');
	$Annonce->NO_MANDAT = request('no_mandat');
	$Annonce->TYPE_MANDAT = request('type_mandat');
	$Annonce->ADR = request('adr');
	$Annonce->SUITE_ADR = request('suite_adr');
	$Annonce->CP = request('cp');
	$Annonce->VILLE = request('ville');
	$Annonce->PAYS = request('pays');
	$Annonce->TBL = request('tbl');
	$Annonce->DEP_G = request('dep_g');
	$Annonce->CP_WEB = request('cp_web');
	$Annonce->VILLE_WEB = request('ville_web');
	$Annonce->MODE_CHAUFF = request('mode_chauff');
	$Annonce->NOM_GEST = request('nom_gest');
	$Annonce->REG_FISC = request('reg_fisc');
	$Annonce->CAT = request('cat');
	$Annonce->TYPE_CHAUFF = request('type_chauff');
	$Annonce->NAT_CHAUFF = request('nat_chauff');
	$Annonce->STANDING = request('standing');
	$Annonce->COUV = request('couv');
	$Annonce->FACADE = request('facade');
	$Annonce->ETAT_EXT = request('etat_ext');
	$Annonce->URL_VIDEO = request('url_video');
	$Annonce->TYPE_CONS = request('type_cons');
	$Annonce->TX_HONO = request('tx_hono');
	$Annonce->SURF_TERR = request('surf_terr');
	$Annonce->SURF_TOT = request('surf_tot');
	$Annonce->SURF_MINI = request('surf_mini');
	$Annonce->TYPE_BIEN = request('type_bien');
	$Annonce->ETAT_AVANC = request('etat_avanc');
	$Annonce->NB_ETAGE = request('nb_etage');
	$Annonce->NB_PARK_INT = request('nb_park_int');
	$Annonce->NB_PARK_EXT = request('nb_park_ext');
	$Annonce->NB_BOX = request('nb_box');
	$Annonce->ANNEE_CONS = request('annee_cons');
	$Annonce->ASCE = request('asce');
	$Annonce->HAND = request('hand');
	$Annonce->INTERPHONE = request('interphone');
	$Annonce->DIGICODE = request('digicode');
	$Annonce->REGL_LOYER = request('regl_loyer');
	$Annonce->LAM_MIN = request('lam_min');
	$Annonce->LAM_MAX = request('lam_max');
	$Annonce->F_HONO = request('f_hono');
	$Annonce->CMA_MIN = request('cma_min');
	$Annonce->CMA_MAX = request('cma_max');
	$Annonce->LOYER_AN_HTHC = request('loyer_an_hthc');
	$Annonce->TBMA = request('tbma');
	$Annonce->TFMA = request('tfma');
	$Annonce->TPIA = request('lpia');
	$Annonce->LPEA = request('lpea');
	$Annonce->REPRISE = request('reprise');
	$Annonce->U_BUR = request('u_bur');
	$Annonce->U_COMM = request('u_comm');
	$Annonce->U_ACT = request('u_act');
	$Annonce->U_PROF = request('u_prof');
	$Annonce->CC = request('cc');
	$Annonce->MC = request('mc');
	$Annonce->TELESURV = request('telesurv');
	$Annonce->DATE_CREATION = request('date_cration');
	$Annonce->DATE_MODIF = request('date_modif');
	$Annonce->DATE_MAND = request('date_mand');
	$Annonce->DATE_FIN_MAND = request('date_fin_mand');
	$Annonce->DATE_LIBER = request('date_liber');
	$Annonce->DATE_CONGE = request('date_conge');
	$Annonce->DATE_BAIL = request('date_bail');
	$Annonce->DATE_DISP = request('date_disp');
	$Annonce->TXT_INTERNET = request('txt_internet');
	$Annonce->ETAT_INT = request('etat_int');
	$Annonce->NB_NIV = request('nb_niv');
	$Annonce->TX_TAXE_PROF = request('tx_taxe_prof');
	$Annonce->LON_VIT = request('lon_vit');
	$Annonce->CDAB = request('cdab');
	$Annonce->GARDIENNAGE = request('gardiennage');
	$Annonce->PREST = request('prestige');
	$Annonce->IMM_COL = request('imm_col');
	$Annonce->IMM_INDEP = request('imm_indep');
	$Annonce->CLIM = request('clim');
	$Annonce->RIE = request('rie');
	$Annonce->IFB = request('ifb');
	$Annonce->NET_TAXE = request('net_taxe');
	$Annonce->NO_CDAB = request('no_cdab');
	$Annonce->SECTEUR = request('secteur');
	$Annonce->CRIT_COMP = request('crit_com');
	$Annonce->DESSERTE = request('desserte');
	$Annonce->HSP = request('hsp');
	$Annonce->SURF_BUR = request('surf_bur');
	$Annonce->DATE_INIT_PH = request('date_init_ph');
	$Annonce->NOM_CONTACT = request('nom_contact');
	$Annonce->TEL_CONTACT = request('tel_contact');
	$Annonce->DIST = request('dist');
	$Annonce->CH_HONO = request('ch_hono');
	$Annonce->LAT = request('lat');
	$Annonce->LON = request('lon');
	$Annonce->PRIX_CONF = request('prix_conf');
	$Annonce->MOTS_CLES = request('mot_cle');
	$Annonce->CONTACT = request('contact');
	$Annonce->AGENCE = request('agence');
	$Annonce->LANGUES = request('langue');
	$Annonce->id = 1;
		
	$Annonce->save();
	
	return view('AjoutReussi');
});

Route::get('/AjoutAnnonceType14', function () {
    return view('Ajout/AjoutBienType14');
})->middleware('auth');
Route::post('/AjoutAnnonceType14', function () {
	$previous = new App\annonce;
	$previous->NO_ASP = request('noASP');
	$previous->id = 1;
	
	$previous->save();
	
	$Annonce = new App\annoncestype14;
	$Annonce->NO_ASP = request('noASP');
	$Annonce->CODE_SOCIETE = request('code_societe');
	$Annonce->CODE_SITE = request('code_site');
	$Annonce->NO_DOSSIER = request('no_dossier');
	$Annonce->NO_RUE = request('no_rue');
	$Annonce->TYPE_RUE = request('type_rue');
	$Annonce->NO_MANDAT = request('no_mandat');
	$Annonce->TYPE_MANDAT = request('type_mandat');
	$Annonce->ADR = request('adr');
	$Annonce->SUITE_ADR = request('suite_adr');
	$Annonce->CP = request('cp');
	$Annonce->VILLE = request('ville');
	$Annonce->PAYS = request('pays');
	$Annonce->QUARTIER = request('quartier');
	$Annonce->NOM_PARK = request('nom_park');
	$Annonce->TRANSPORT = request('transport');
	$Annonce->PROXIMITE = request('proximite');
	$Annonce->SITUATION = request('situation');
	$Annonce->CP_WEB = request('cp_web');
	$Annonce->VILLE_WEB = request('ville_web');
	$Annonce->CAT = request('cat');
	$Annonce->URL_VIDEO = request('url_video');
	$Annonce->TYPE_CONS = request('type_cons');
	$Annonce->ETAT_GEN = request('etat_gen');
	$Annonce->DIGICODE = request('digicode');
	$Annonce->REF_ADB = request('ref_adb');
	$Annonce->LOYER_HC = request('loyer_hc');
	$Annonce->PROV_CH = request('prov_ch');
	$Annonce->TVA = request('tva');
	$Annonce->LOYER_TTC = request('loyerttc');
	$Annonce->CAUTION_BIP = request('caution_bip');
	$Annonce->DEP_G = request('dep_g');
	$Annonce->HRATTC = request('hrattc');
	$Annonce->SURF = request('surf');
	$Annonce->TX_TVA = request('tx_tva');
	$Annonce->TYPE_BIEN = request('type_bien');
	$Annonce->ETAT_AVANC = request('etat_avanc');
	$Annonce->NIVEAU = request('niveau');
	$Annonce->NB_NIV = request('nb_niv');
	$Annonce->TYPE_STAT = request('type_stat');
	$Annonce->TYPE_ACCES = request('type_acces');
	$Annonce->ACCES_PIETON = request('acces_pieton');
	$Annonce->NB_VOITURE = request('nb_voiture');
	$Annonce->ANNEE_CONS = request('annee_cons');
	$Annonce->LOYER_MT = request('loyer_mt');
	$Annonce->ASCE = request('asce');
	$Annonce->HAND = request('hand');
	$Annonce->BIP = request('bip');
	$Annonce->CARTE_MAGN = request('carte_magn');
	$Annonce->VIDEO_SURV = request('video_surv');
	$Annonce->CC = request('cc');
	$Annonce->DATE_CREATION = request('date_cration');
	$Annonce->DATE_MODIF = request('date_modif');
	$Annonce->DATE_MAND = request('date_mand');
	$Annonce->DATE_FIN_MAND = request('date_fin_mand');
	$Annonce->DATE_LIBER = request('date_liber');
	$Annonce->DATE_DISP = request('date_disp');
	$Annonce->DATE_FIN_BAIL = request('date_fin_bail');
	$Annonce->TXT_INTERNET = request('txt_internet');
	$Annonce->DUREE_BAIL = request('duree_bail');
	$Annonce->LONGUEUR = request('longeur');
	$Annonce->LARGEUR = request('largeur');
	$Annonce->HAUTEUR = request('hauteur');
	$Annonce->GARDIENNAGE = request('gardiennage');
	$Annonce->HONO_LOC_TTC = request('hono_loc_ttc');
	$Annonce->TOT_HONO_LOC_TTC = request('tot_hono_loc_ttc');
	$Annonce->SECTEUR = request('secteur');
	$Annonce->CRIT_COMP = request('crit_com');
	$Annonce->CH_MT = request('ch_mt');
	$Annonce->DATE_INIT_PH = request('date_init_ph');
	$Annonce->NOM_CONTACT = request('nom_contact');
	$Annonce->TEL_CONTACT = request('tel_contact');
	$Annonce->DIST = request('dist');
	$Annonce->LAT = request('lat');
	$Annonce->LON = request('lon');
	$Annonce->PRIX_CONF = request('prix_conf');
	$Annonce->MOTS_CLES = request('mot_cle');
	$Annonce->CONTACT = request('contact');
	$Annonce->AGENCE = request('agence');
	$Annonce->LANGUES = request('langue');
	$Annonce->INDICE_NATURE_CHARGE = request('indice_nature_charge');
	$Annonce->INDICE_REGLEMENT_LOYER = request('indice_complement_loyer');
	$Annonce->INDICE_REGLEMENT_CHARGES = request('indice_reglement_charge');
	$Annonce->HONO_ETAT_LIEU_LOC = request('hono_etat_lieu_loc');
	$Annonce->id = 1;
		
	$Annonce->save();
	
	return view('AjoutReussi');
});

Route::get('/AjoutAnnonceType21', function () {
    return view('Ajout/AjoutBienType21');
})->middleware('auth');
Route::post('/AjoutAnnonceType21', function () {
	$previous = new App\annonce;
	$previous->NO_ASP = request('noASP');
	$previous->id = 1;
	
	$previous->save();
	
	$Annonce = new App\annoncestype21;
	$Annonce->NO_ASP = request('noASP');
	$Annonce->CODE_SOCIETE = request('code_societe');
	$Annonce->CODE_SITE = request('code_site');
	$Annonce->NO_DOSSIER = request('no_dossier');
	$Annonce->NO_RUE = request('no_rue');
	$Annonce->TYPE_RUE = request('type_rue');
	$Annonce->NO_MANDAT = request('no_mandat');
	$Annonce->TYPE_MANDAT = request('type_mandat');
	$Annonce->ADR = request('adr');
	$Annonce->SUITE_ADR = request('suite_adr');
	$Annonce->CP = request('cp');
	$Annonce->VILLE = request('ville');
	$Annonce->PAYS = request('pays');
	$Annonce->QUARTIER = request('quartier');
	$Annonce->RESIDENCE = request('residence');
	$Annonce->TRANSPORT = request('transport');
	$Annonce->PROXIMITE = request('proximite');
	$Annonce->SITUATION = request('situation');
	$Annonce->NOM_PROM = request('nom_prom');
	$Annonce->NOM_PRG = request('nom_prg');
	$Annonce->URL_VIDEO = request('url_video');
	$Annonce->TYPE1 = request('type1');
	$Annonce->TYPE2 = request('type2');
	$Annonce->TYPE3 = request('type3');
	$Annonce->TYPE4 = request('type4');
	$Annonce->PRIX_MIN = request('prix_mini');
	$Annonce->TX_HONO = request('tx_hono');
	$Annonce->PRIX_MAX = request('prix_max');
	$Annonce->TYPE_BIEN = request('type_bien');
	$Annonce->ETAT_AVANC = request('etat_avanc');
	$Annonce->PCE_MIN = request('pce_min');
	$Annonce->PCE_MAX = request('pce_max');
	$Annonce->COLLIND = request('collind');
	$Annonce->NB_TYPE1 = request('nb_type1');
	$Annonce->NB_TYPE2 = request('nb_type2');
	$Annonce->NB_TYPE3 = request('nb_type3');
	$Annonce->NB_TYPE4 = request('nb_type4');
	$Annonce->NB_TYPE5 = request('nb_type5');
	$Annonce->NB_TYPE6 = request('nb_type6');
	$Annonce->CC = request('cc');
	$Annonce->DATE_CREATION = request('date_cration');
	$Annonce->DATE_MODIF = request('date_modif');
	$Annonce->DATE_MAND = request('date_mand');
	$Annonce->DATE_FIN_MAND = request('date_fin_mand');
	$Annonce->TXT_INTERNET = request('txt_internet');
	$Annonce->TYPE5 = request('type5');
	$Annonce->TYPE6 = request('type6');
	$Annonce->SECTEUR = request('secteur');
	$Annonce->CRIT_COMP = request('crit_com');
	$Annonce->PRIX_CONF = request('prix_conf');
	$Annonce->MOTS_CLES = request('mot_cle');
	$Annonce->CANAUX = request('canaux');
	$Annonce->FILTRES = request('filtres');
	$Annonce->LOTNEUFS = request('lots_neuf');
	$Annonce->CONTACT = request('contact');
	$Annonce->AGENCE = request('agence');
	$Annonce->LANGUES = request('langues');
	$Annonce->id = 1;
		
	$Annonce->save();
	
	return view('AjoutReussi');
});

Route::get('/AjoutLotsNeufs', function () {
    return view('Ajout/AjoutLotsNeufs');
})->middleware('auth');
Route::post('/AjoutLotsNeufs', function () {
	$previous = new App\annonce;
	$previous->NO_ASP = request('noASP');
	$previous->id = 1;
	
	$previous->save();
	
	$Annonce = new App\lotsneuf;
	$Annonce->NO_ASP = request('noASP');
	$Annonce->ETAT_AVANC = request('etat_avanc');
	$Annonce->TYPE_BIEN = request('type_bien');
	$Annonce->CAT = request('cat');
	$Annonce->NO_LOT = request('no_lot');
	$Annonce->NB_PCE = request('nb_pce');
	$Annonce->NB_CHB = request('nb_chb');
	$Annonce->SURF = request('surf');
	$Annonce->ETAGE = request('etage');
	$Annonce->NB_ETAGE = request('nb_etage');
	$Annonce->SURF_JAR = request('surf_jar');
	$Annonce->SURF_TERR = request('surf_terr');
	$Annonce->VUE = request('vue');
	$Annonce->EXPO = request('expo');
	$Annonce->PV = request('pv');
	$Annonce->DATE_LIV = request('date_liv');
	$Annonce->NB_PARK = request('nb_park');
	$Annonce->TYPE_PARK = request('type_park');
	$Annonce->PRIX_PARK = request('prix_park');
	$Annonce->LOYER_GAR = request('loyer_gar');
	$Annonce->RENTAB = request('rentab');
	$Annonce->NO_PLACE = request('no_place');
	$Annonce->TXT_INTERNET = request('txt_internet');
	$Annonce->BAT = request('bat');
	$Annonce->id = 1;
		
	$Annonce->save();
	
	return view('AjoutReussi');
});


//Contacts
Route::get('/Contacts', function () {
    return view('Contacts');
});

Route::get('/AjoutContacts', function () {
    return view('Ajout/AjoutContact');
})->middleware('auth');

Route::post('/AjoutContacts', function () {
	$Annonce = new App\Contact;
	$Annonce->id = 1;
	$Annonce->CIVILITE = request('civilite');
	$Annonce->NOM = request('nom');
	$Annonce->PRENOM = request('prenom');
	$Annonce->ADRESSE = request('adresse');
	$Annonce->SUITE_ADRESSE = request('suite_adresse');
	$Annonce->CP = request('cp');
	$Annonce->VILLE = request('ville');
	$Annonce->PAYS = request('pays');
	$Annonce->TEL = request('tel');
	$Annonce->FAX = request('fax');
	$Annonce->MOBILE = request('mobile');
	$Annonce->MAIL = request('mail');
	$Annonce->NO_ASP = request('no_asp');
	$Annonce->LIBELLE = request('libelle');
	$Annonce->FONCTION = request('fonction');
	$Annonce->URL_PHOTO = request('url_photo');
	
	$Annonce->save();
	
	return view('AjoutReussi');
	
});

//Agences
Route::get('/Agences', function () {
    return view('Agences');
});
Route::get('/AjoutAgence', function () {
    return view('Ajout/AjoutAgence');
})->middleware('auth');

Route::post('/AjoutAgence', function () {
	$Annonce = new App\agence;
	$Annonce->CODE_SOCIETE = request('code_societe');
	$Annonce->CODE_SITE = request('code_site');
	$Annonce->RAISON_SOCIALE = request('raison_sociale');
	$Annonce->ENSEIGNE = request('enseigne');
	$Annonce->CIVILITE = request('civilite');
	$Annonce->NOM = request('nom');
	$Annonce->PRENOM = request('prenom');
	$Annonce->ADRESSE = request('adresse');
	$Annonce->SUITE_ADRESSE = request('suite_adresse');
	$Annonce->CP = request('cp');
	$Annonce->VILLE = request('ville');
	$Annonce->PAYS = request('pays');
	$Annonce->TEL = request('tel');
	$Annonce->FAX = request('fax');
	$Annonce->MOBILE = request('mobile');
	$Annonce->MAIL = request('mail');
	$Annonce->FORME_JUR = request('forme_jur');
	$Annonce->SIRET = request('siret');
	$Annonce->NO_CARTEPRO_T = request('no_cartepro_t');
	$Annonce->NO_CARTEPRO_G = request('no_cartepro_g');
	$Annonce->GROUPE = request('groupe');
	$Annonce->SYNDICAT = request('syndicat');
	$Annonce->CAISSE_GARENTIE = request('caisse_garantie');
	$Annonce->CAPITAL_SOCIAL = request('capital_social');
	$Annonce->MONTANT_GARENTIE_T = request('mt_garantie_t');
	$Annonce->MONTANT_GARENTIE_G = request('mt_garantie_g');
	$Annonce->DATE_CREAT = request('date_creat');
	$Annonce->DATE_MODIF = request('date_modif');
	$Annonce->NO_ASP = request('no_asp');
	$Annonce->RESEAU = request('reseau');
	$Annonce->WEB = request('web');
	$Annonce->INFO_JUR = request('info_jur');
	$Annonce->SECONDE_DEVISE = request('seconde_devise');
	$Annonce->TX_CONV_SEC_DEVISE = request('tx_conv_sec_dev');
	$Annonce->TEL_LOC = request('tel_loc');
	$Annonce->MAIL_LOC = request('mail_loc');
	$Annonce->PREF_CARTE_PRO = request('pref_cartepro');
	$Annonce->ADR_SS = request('adresse_ss');
	$Annonce->SUITE_ADR_SS = request('suite_adresse_ss');
	$Annonce->CP_SS = request('cp_ss');
	$Annonce->VILLE_SS = request('ville_ss');
	$Annonce->PAYS_SS = request('pays_ss');
	$Annonce->ADR_GARANT = request('adr_garant');
	$Annonce->SUITE_ADR_GARANT = request('suite_adr_garant');
	$Annonce->CP_GARANT = request('cp_garant');
	$Annonce->VILLE_GARANT = request('ville_garant');
	$Annonce->GARENTIE_MF = request('garentie_mf');
	$Annonce->TYPE_CARTE_PRO = request('type_cartepro');
	$Annonce->TYPE_GARENTIE = request('type_garantie');
	$Annonce->NO_TVA_INTRA = request('no_tva_intra');
	$Annonce->ADR_PREF = request('adr_pref');
	$Annonce->SUITE_ADR_PREF = request('suite_adr_pref');
	$Annonce->CP_PREF = request('cp_pref');
	$Annonce->VILLE_PREF = request('ville_pref');
	$Annonce->LAT = request('lat');
	$Annonce->LON = request('lon');
	$Annonce->ID_TIERS = request('id_tiers');
	$Annonce->NOM_GARENTIE_G = request('nom_garantie_g');
	$Annonce->ADR_GARENTIE_G = request('adresse_garantie_g');
	$Annonce->SUITE_ADR_GARENTIE_G = request('suite_adresse_garantie_g');
	$Annonce->CP_GARENTIE_G = request('cp_garantie_g');
	$Annonce->VILLE_GARENTIE_G = request('ville_garantie_g');
	$Annonce->id = 1;
	
	$Annonce->save();
	
	return view('AjoutReussi');
});
//Compte Rendu
Route::get('/CompteRendu', function () {
    return view('Consultation/AfficheCompteRendu');
});
Route::get('/AjoutCompteRendu', function () {
    return view('Ajout/AjoutCompteRendu');
})->middleware('auth');

Route::post('/AjoutCompteRendu', function () {
	$Annonce = new App\compterendu;
	$Annonce->NO_COMPTE_RENDU = request('no_compte_rendu');
	$Annonce->DATE_VISITE = request('date_visite');
	$Annonce->PROPRIETAIRE = request('proprietaire');
	$Annonce->ANNONCE = request('annonce');
	$Annonce->VISITEURS = request('visiteurs');
	$Annonce->REMARQUE = request('remarque');
	$Annonce->FONCTION_VISITEUR = request('fonction_visiteur');
	$Annonce->FINANCEMENT_PREVU = request('financement_prevu');
	$Annonce->NOTE_VISITEUR = request('note_visiteur');
	$Annonce->CONTACT = request('contact');
	$Annonce->SYNTHESE = request('synthese');
	$Annonce->id = 1;
	
	$Annonce->save();
	
	return view('AjoutReussi');
});

//Auth
Auth::routes();


Route::get('/AjoutAgence', 'DefaultController@index')->middleware('App\Http\Middleware\Auth');


