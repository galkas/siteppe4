<ul id="menu-demo2">
	 <li><a href="/ppe4/public/Accueil">Accueil</a></li>
	<li><a>Ajout des Annonces</a>
		<ul>
			<li><a href="/ppe4/public/AjoutAnnonceType1">Ajout d'une annonce type 1</a></li>
			<li><a href="/ppe4/public/AjoutAnnonceType2">Ajout d'une annonce type 2</a></li>
                        <li><a href="/ppe4/public/AjoutAnnonceType3">Ajout d'une annonce type 3</a></li>
			<li><a href="/ppe4/public/AjoutAnnonceType4">Ajout d'une annonce type 4</a></li>
			<li><a href="/ppe4/public/AjoutAnnonceType5">Ajout d'une annonce type 5</a></li>
			<li><a href="/ppe4/public/AjoutAnnonceType6">Ajout d'une annonce type 6</a></li>
			<li><a href="/ppe4/public/AjoutAnnonceType7">Ajout d'une annonce type 7</a></li>
			<li><a href="/ppe4/public/AjoutAnnonceType11">Ajout d'une annonce type 11</a></li>
			<li><a href="/ppe4/public/AjoutAnnonceType12">Ajout d'une annonce type 12</a></li>
			<li><a href="/ppe4/public/AjoutAnnonceType13">Ajout d'une annonce type 13</a></li>
			<li><a href="/ppe4/public/AjoutAnnonceType14">Ajout d'une annonce type 14</a></li>
			<li><a href="/ppe4/public/AjoutAnnonceType21">Ajout d'une annonce type 21</a></li>
			<li><a href="/ppe4/public/AjoutLotsNeufs">Ajout d'un lot neuf</a></li>
		</ul>
	</li>
	<li><a>Consultation des Annonces</a>
		<ul>
			<li><a href="/ppe4/public/AfficheAnnonceType1">Annonces type 1</a></li>
			<li><a href="/ppe4/public/AfficheAnnonceType2">Annonces type 2</a></li>
                        <li><a href="/ppe4/public/AfficheAnnonceType3">Annonces type 3</a></li>
			<li><a href="/ppe4/public/AfficheAnnonceType4">Annonces type 4</a></li>
			<li><a href="/ppe4/public/AfficheAnnonceType5">Annonces type 5</a></li>
			<li><a href="/ppe4/public/AfficheAnnonceType6">Annonces type 6</a></li>
			<li><a href="/ppe4/public/AfficheAnnonceType7">Annonces type 7</a></li>
			<li><a href="/ppe4/public/AfficheAnnonceType11">Annonces type 11</a></li>
			<li><a href="/ppe4/public/AfficheAnnonceType12">Annonces type 12</a></li>
			<li><a href="/ppe4/public/AfficheAnnonceType13">Annonces type 13</a></li>
			<li><a href="/ppe4/public/AfficheAnnonceType14">Annonces type 14</a></li>
			<li><a href="/ppe4/public/AfficheAnnonceType21">Annonces type 21</a></li>
			<li><a href="/ppe4/public/AfficheLotsNeufs">Lot neufs</a></li>
		</ul>
	</li>
	<li><a href="/ppe4/public/Contacts">Gestion des Contacts</a>
		<ul>
			<li><a href="/ppe4/public/AjoutContacts">Ajout de contact</a></li>
		</ul>
	</li>
	<li><a href="/ppe4/public/Agences">Gestion Agences</a>
		<ul>
			<li><a href="/ppe4/public/AjoutAgence">Ajout Agence</a></li>
		</ul>
	</li>
	<li><a href="/ppe4/public/CompteRendu">Compte Rendu</a>
		<ul>
			<li><a href="/ppe4/public/AjoutCompteRendu">Ajout Compte Rendu</a></li>
		</ul>
	</li>
</ul>
