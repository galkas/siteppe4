<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="{{ asset('css/css.css') }}">
</head>
<body>
<div class="accueil">
        <center><h1>Ajout Agence<h1/></center>
@include("../BarreNavigation")
</br>
</div>
<div class="formulairePersonne">
<ul>
<form action="AjoutAgence" method="post">
{{ csrf_field() }}
<input type="text" size="4" name="code_societe" placeholder="code société" required/> 
<input type="text" size="2" name="code_site" placeholder="code site" required/> 
<input type="text" size="32" name="raison_sociale" placeholder="raison sociale" required/> 
<input type="text" size="32" name="enseigne" placeholder="enseigne" required/> 
<input type="text" size="15" name="civilite" placeholder="civilité" required/> 
<input type="text" size="32" name="nom" placeholder="nom" required/> 
<input type="text" size="20" name="prenom" placeholder="prénom" required/> 
<input type="text" size="32" name="adresse" placeholder="adresse" required/> 
<input type="text" size="32" name="suite_adresse" placeholder="suite adresse" required/> 
<input type="text" size="5" name="cp" placeholder="code postal" required/> 
<input type="text" size="26" name="ville" placeholder="ville" required/> 
<input type="text" size="25" name="pays" placeholder="pays" required/> 
<input type="text" size="20" name="tel" placeholder="téléphone" required/> 
<input type="text" size="20" name="fax" placeholder="fax" required/> 
<input type="text" size="20" name="mobile" placeholder="mobile" required/> 
<input type="text" size="50" name="mail" placeholder="mail" required/> 
<input type="text" size="5" name="forme_jur" placeholder="forme juridique" required/> 
<input type="text" size="40" name="siret" placeholder="siret" required/> 
<input type="text" size="10" name="no_cartepro_t" placeholder="no carte pro. transac" required/> 
<input type="text" size="10" name="no_cartepro_g" placeholder="no carte pro. gestion" required/> 
<input type="text" size="25" name="groupe" placeholder="groupe" required/> 
<input type="text" size="25" name="syndicat" placeholder="syndicat" required/> 
<input type="text" size="25" name="caisse_garantie" placeholder="caisse garantie" required/> 
<input type="text" size="25" name="capital_social" placeholder="capital social" required/> 
<input type="text" size="25" name="mt_garantie_t" placeholder="montant garantie transac." required/> 
<input type="text" size="25" name="mt_garantie_g" placeholder="montant garantie gestion" required/> 
<input type="date" name="date_creat" required/> date création    
<input type="date" name="date_modif" required/> date modification    
<input type="text" size="25" name="no_asp" placeholder="numéro asp" required/> 
<input type="text" size="25" name="reseau" placeholder="réseau" required/> 
<input type="text" size="50" name="web" placeholder="web" required/> 
<input type="text" size="1024" name="info_jur" placeholder="info juridique" required/> 
<input type="text" size="5" name="seconde_devise" placeholder="seconde devise" required/> 
<input type="text" size="25" name="tx_conv_sec_dev" placeholder="taux convertion sec.devise" required/> 
<input type="text" size="20" name="tel_loc" placeholder="téléphone location" required/> 
<input type="text" size="50" name="mail_loc" placeholder="mail location" required/> 
<input type="text" size="26" name="pref_cartepro" placeholder="préfecture carte pro." required/> 
<input type="text" size="32" name="adresse_ss" placeholder="Adresse ss" required/> 
<input type="text" size="32" name="suite_adresse_ss" placeholder="suite adresse ss" required/> 
<input type="text" size="5" name="cp_ss" placeholder="code postal ss" required/> 
<input type="text" size="26" name="ville_ss" placeholder="ville ss" required/> 
<input type="text" size="25" name="pays_ss" placeholder="pays ss" required/> 
<input type="text" size="32" name="adr_garant" placeholder="adresse garant" required/> 
<input type="text" size="32" name="suite_adr_garant" placeholder="suite adresse garant" required/> 
<input type="text" size="5" name="cp_garant" placeholder="code postal garant" required/> 
<input type="text" size="26" name="ville_garant" placeholder="ville garant" required/> 
<input type="hidden" name="garentie_mf" value="0"/>   
<input type="checkbox" name="garentie_mf" value="1"/> garentie avec maniement de fonds    
<input type="text" size="25" name="type_cartepro" placeholder="type carte pro." required/> 
<input type="text" size="25" name="type_garantie" placeholder="type garantie" required/> 
<input type="text" size="25" name="no_tva_intra" placeholder="no tva intracommunotaire" required/> 
<input type="text" size="32" name="adr_pref" placeholder="adresse préfecture" required/> 
<input type="text" size="32" name="suite_adr_pref" placeholder="suite adresse préfecture" required/> 
<input type="text" size="5" name="cp_pref" placeholder="code postal préfecture" required/> 
<input type="text" size="26" name="ville_pref" placeholder="ville préfecture" required/> 
<input type="text" size="25" name="lat" placeholder="latitude" required/> 
<input type="text" size="25" name="lon" placeholder="longitude" required/> 
<input type="text" size="25" name="id_tiers" placeholder="id tiers" required/> 
<input type="text" size="25" name="nom_garantie_g" placeholder="nom garantie G" required/> 
<input type="text" size="32" name="adresse_garantie_g" placeholder="adresse garantie G" required/> 
<input type="text" size="32" name="suite_adresse_garantie_g" placeholder="suite adresse garantie G" required/> 
<input type="text" size="5" name="cp_garantie_g" placeholder="code postal garantie G" required/> 
<input type="text" size="26" name="ville_garantie_g" placeholder="ville garantie G" required/> 
<input type="text" size="100" name="annonces" placeholder="annonces" required/> 
</br>
<br/>
<center><input type="submit" value="Valider le formulaire"/></center>
</form>

</ul>
</div>
