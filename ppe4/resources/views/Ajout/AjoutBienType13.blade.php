<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="{{ asset('css/css.css') }}">
</head>
<body>
<div class="accueil">
        <center><h1>Ajout de bien type 13<h1/></center>
@include("../BarreNavigation")
</br>
</div>
<div class="formulairePersonne">
<ul>
<form action="AjoutAnnonceType13" method="post">
{{ csrf_field() }}
<input type="text" size="25" name="noASP" placeholder="numéro ASP" required/> 
<input type="text" size="4" name="code_societe" placeholder="n°société" required/>
<input type="text" size="2" name="code_site" placeholder="numéro site" required/> 
<input type="text" size="5" name="no_dossier" placeholder="numéro dossier" required/> 
<input type="text" size="10" name="no_rue" placeholder="numéro rue" required/> 
<input type="text" size="15" name="type_rue" placeholder="type de rue" required/> 
<input type="text" size="10" name="no_mandat" placeholder="numéro de mandat" required/> 
<input type="text" size="15" name="type_mandat" placeholder="type de mandat" required/> 
<input type="text" size="32" name="adr" placeholder="adresse" required/> 
<input type="text" size="32" name="suite_adr" placeholder="suite adresse" required/> 
<input type="text" size="5" name="cp" placeholder="code postal" required/> 
<input type="text" size="26" name="ville" placeholder="ville" required/> 
<input type="text" size="25" name="pays" placeholder="pays" required/> 
<input type="text" size="25" name="tbl" placeholder="type bail location" required/> 
<input type="text" size="25" name="dep_g" placeholder="dépos garantie" required/> 
<input type="text" size="5" name="cp_web" placeholder="cp internet" required/> 
<input type="text" size="26" name="ville_web" placeholder="ville internet" required/> 
<input type="text" size="25" name="mode_chauff" placeholder="mode chauffage" required/> 
<input type="text" size="32" name="nom_gest" placeholder="nom gestionnaire" required/> 
<input type="text" size="25" name="reg_fisc" placeholder="régime fiscal" required/> 
<input type="text" size="25" name="cat" placeholder="catégorie (sous type de bien)" required/> 
<input type="text" size="25" name="type_chauff" placeholder="type chauffage" required/> 
<input type="text" size="25" name="nat_chauff" placeholder="nature chauffage" required/> 
<input type="text" size="25" name="standing" placeholder="standing" required/> 
<input type="text" size="25" name="couv" placeholder="couverture" required/> 
<input type="text" size="25" name="facade" placeholder="façade" required/> 
<input type="text" size="25" name="etat_ext" placeholder="état extérieur" required/> 
<input type="text" size="150" name="url_video" placeholder="url visite/video" required/> 
<input type="text" size="25" name="type_cons" placeholder="type construction" required/> 
<input type="text" size="25" name="tx_hono" placeholder="taux honoraires" required/> 
<input type="text" size="25" name="surf_terr" placeholder="surface terrain" required/> 
<input type="text" size="25" name="surf_tot" placeholder="surface totale" required/> 
<input type="text" size="25" name="surf_mini" placeholder="suface mini" required/> 
<input type="text" size="25" name="type_bien" placeholder="type de bien" required/> 
<input type="text" size="25" name="etat_avanc" placeholder="état avancement" required/> 
<input type="text" size="25" name="nb_etage" placeholder="nombres d'étages" required/> 
<input type="text" size="25" name="nb_park_int" placeholder="nombre parking intérieur" required/>
<input type="text" size="25" name="nb_park_ext" placeholder="nombre parking extérieur" required/> 
<input type="text" size="25" name="nb_box" placeholder="nombre box" required/> 
<input type="text" size="25" name="annee_cons" placeholder="année construction" required/> 
<input type="hidden" name="asce" value="0"/>   
<input type="checkbox" name="asce" value="1"/> ascenseur    
<input type="hidden" name="hand" value="0"/>   
<input type="checkbox" name="hand" value="1"/> accès handicapé    
<input type="hidden" name="interphone" value="0"/>   
<input type="checkbox" name="interphone" value="1"/> interphone    
<input type="hidden" name="digicode" value="0"/>   
<input type="checkbox" name="digicode" value="1"/> digicode    
<input type="text" size="30" name="regl_loyer" placeholder="règlement loyer" required/> 
<input type="text" size="25" name="lam_min" placeholder="loyer annuel /m2 min" required/> 
<input type="text" size="25" name="lam_max" placeholder="loyer annuel /m2 max" required/> 
<input type="text" size="25" name="f_hono" placeholder="forfait honoraire" required/> 
<input type="text" size="25" name="cma_min" placeholder="charges /m2 /ans min" required/> 
<input type="text" size="25" name="cma_max" placeholder="charges /m2 /ans max" required/> 
<input type="text" size="25" name="loyer_an_hthc" placeholder="loyer annuel HT/HC" required/> 
<input type="text" size="25" name="tbma" placeholder="taxe bureau /m2/an" required/> 
<input type="text" size="25" name="tfma" placeholder="taxe fonçière /m2/an" required/> 
<input type="text" size="25" name="lpia" placeholder="loyer park int/an" required/> 
<input type="text" size="25" name="lpea" placeholder="loyer park ext/an" required/> 
<input type="text" size="25" name="reprise" placeholder="reprise" required/> 
<input type="hidden" name="u_bur" value="0"/>   
<input type="checkbox" name="u_bur" value="1"/> usage bureau    
<input type="hidden" name="u_comm" value="0"/>   
<input type="checkbox" name="u_comm" value="1"/> usage commerce    
<input type="hidden" name="u_act" value="0"/>   
<input type="checkbox" name="u_act" value="1"/> usage activité    
<input type="hidden" name="u_prof" value="0"/>   
<input type="checkbox" name="u_prof" value="1"/> usage professionnel    
<input type="hidden" name="cc" value="0"/>   
<input type="checkbox" name="cc" value="1"/> coup de coeur    
<input type="hidden" name="mc" value="0"/>   
<input type="checkbox" name="mc" value="1"/> monte charge    
<input type="hidden" name="telesurv" value="0"/>   
<input type="checkbox" name="telesurv" value="1"/> télésurveillance    
<input type="date" name="date_cration" required/> date dossier    
<input type="date" name="date_modif" required/> date modification    
<input type="date" name="date_mand" required/> date mandat    
<input type="date" name="date_fin_mand" required/> date fin mandat    
<input type="date" name="date_liber" required/> date libération    
<input type="date" name="date_conge" required/> date congé    
<input type="date" name="date_bail" required/> date bail    
<input type="date" name="date_disp" required/> date disponibilité    
<input type="text" size="1024" name="txt_internet" placeholder="texte annonce FR" required/> 
<input type="text" size="25" name="etat_int" placeholder="état intérieur" required/> 
<input type="text" size="25" name="nb_niv" placeholder="nombre niveau" required/> 
<input type="text" size="25" name="tx_taxe_prof" placeholder="taux taxe pro." required/> 
<input type="text" size="25" name="lon_vit" placeholder="longueur vitrine" required/> 
<input type="text" size="25" name="cdab" placeholder="cession droit au bail" required/> 
<input type="hidden" name="gardiennage" value="0"/>   
<input type="checkbox" name="gardiennage" value="1"/> gardiennage    
<input type="hidden" name="prestige" value="0"/>   
<input type="checkbox" name="prestige" value="1"/> prestige    </br>
<input type="hidden" name="imm_col" value="0"/>   
<input type="checkbox" name="imm_col" value="1"/> immbeuble collectif    
<input type="hidden" name="imm_indep" value="0"/>   
<input type="checkbox" name="imm_indep" value="1"/> immeuble independant    
<input type="hidden" name="clim" value="0"/>   
<input type="checkbox" name="clim" value="1"/> climatisation    
<input type="hidden" name="rie" value="0"/>   
<input type="checkbox" name="rie" value="1"/> RIE    
<input type="hidden" name="ifb" value="0"/>   
<input type="checkbox" name="ifb" value="1"/> impot foncier bailleur    
<input type="hidden" name="net_taxe" value="0"/>   
<input type="checkbox" name="net_taxe" value="1"/> net de taxe    
<input type="hidden" name="no_cdab" value="0"/>   
<input type="checkbox" name="no_cdab" value="1"/> pas de cession de bail    
<input type="text" size="100" name="secteur" placeholder="libelle secteur" required/> 
<input type="text" size="512" name="crit_com" placeholder="critères complémentaires" required/> 
<input type="text" size="25" name="desserte" placeholder="dessertes" required/> 
<input type="text" size="25" name="hsp" placeholder="H.S.P." required/> 
<input type="text" size="25" name="surf_bur" placeholder="surface bureau" required/> 
<input type="date" name="date_init_ph" required/> date initialise photo    
<input type="text" size="25" name="nom_contact" placeholder="contact annonce" required/> 
<input type="text" size="20" name="tel_contact" placeholder="téléphone contact annonce" required/> 
<input type="text" size="20" name="dist" placeholder="distance en km ville web" required/> 
<input type="text" size="25" name="ch_hono" placeholder="0=vendeur" required/> 
<input type="text" size="25" name="lat" placeholder="latitude" required/> 
<input type="text" size="25" name="lon" placeholder="longitude" required/> 
<input type="hidden" name="prix_conf" value="0"/>   
<input type="checkbox" name="prix_conf" value="1"/> masquer prix    
<input type="text" size="150" name="mot_cle" placeholder="mots clés" required/> 
<input type="text" size="100" name="contact" placeholder="contacts" required/> 
<input type="text" size="100" name="agence" placeholder="agence" required/> 
<input type="text" size="100" name="langue" placeholder="langues" required/> 
</br>
<br/>
<center><input type="submit" value="Valider le formulaire"/></center>
</form>

</ul>
</div>
