<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="{{ asset('css/css.css') }}">
</head>
<body>
<div class="accueil">
        <center><h1>Ajout de bien type 14<h1/></center>
@include("../BarreNavigation")
</br>
</div>
<div class="formulairePersonne">
<ul>
<form action="AjoutAnnonceType14" method="post">
{{ csrf_field() }}
<input type="text" size="25" name="noASP" placeholder="numéro ASP" required/> 
<input type="text" size="4" name="code_societe" placeholder="n°société" required/>
<input type="text" size="2" name="code_site" placeholder="numéro site" required/> 
<input type="text" size="5" name="no_dossier" placeholder="numéro dossier" required/> 
<input type="text" size="10" name="no_rue" placeholder="numéro rue" required/> 
<input type="text" size="15" name="type_rue" placeholder="type de rue" required/> 
<input type="text" size="10" name="no_mandat" placeholder="numéro de mandat" required/> 
<input type="text" size="15" name="type_mandat" placeholder="type de mandat" required/> 
<input type="text" size="32" name="adr" placeholder="adresse" required/> 
<input type="text" size="32" name="suite_adr" placeholder="suite adresse" required/> 
<input type="text" size="5" name="cp" placeholder="code postal" required/> 
<input type="text" size="26" name="ville" placeholder="ville" required/> 
<input type="text" size="25" name="pays" placeholder="pays" required/> 
<input type="text" size="25" name="quartier" placeholder="quartier" required/> 
<input type="text" size="25" name="nom_park" placeholder="nom parking" required/> 
<input type="text" size="25" name="transport" placeholder="transport" required/> 
<input type="text" size="25" name="proximite" placeholder="proximité" required/> 
<input type="text" size="25" name="situation" placeholder="situation" required/> 
<input type="text" size="5" name="cp_web" placeholder="cp internet" required/> 
<input type="text" size="26" name="ville_web" placeholder="ville internet" required/> 
<input type="text" size="25" name="cat" placeholder="catégorie (sous type de bien)" required/> 
<input type="text" size="150" name="url_video" placeholder="url visite/video" required/> 
<input type="text" size="25" name="type_cons" placeholder="type construction" required/> 
<input type="text" size="25" name="etat_gen" placeholder="état général" required/> 
<input type="text" size="10" name="digicode" placeholder="digicode" required/> 
<input type="text" size="25" name="ref_adb" placeholder="référence ADB" required/> 
<input type="text" size="25" name="loyer_hc" placeholder="loyer H.C" required/>  
<input type="text" size="25" name="prov_ch" placeholder="prov. CH." required/> 
<input type="text" size="25" name="tva" placeholder="tva" required/> 
<input type="text" size="25" name="loyerttc" placeholder="loyer TTC" required/> 
<input type="text" size="25" name="caution_bip" placeholder="caution pour bip" required/> 
<input type="text" size="25" name="dep_g" placeholder="dépôt garantie" required/> 
<input type="text" size="25" name="hrattc" placeholder="hono.rédact.acte TTC" required/> 
<input type="text" size="25" name="surf" placeholder="surface" required/> 
<input type="text" size="25" name="tx_tva" placeholder="taux tva" required/> 
<input type="text" size="25" name="type_bien" placeholder="type de bien" required/> 
<input type="text" size="25" name="etat_avanc" placeholder="état avancement" required/> 
<input type="text" size="25" name="niveau" placeholder="niveau" required/> 
<input type="text" size="25" name="nb_niv" placeholder="nombre niveau" required/> 
<input type="text" size="25" name="type_stat" placeholder="type stationnement" required/> 
<input type="text" size="25" name="type_acces" placeholder="type accès" required/> 
<input type="text" size="25" name="acces_pieton" placeholder="accès piéton" required/> 
<input type="text" size="25" name="nb_voiture" placeholder="nombre voitures" required/> 
<input type="text" size="25" name="annee_cons" placeholder="année construction" required/> 
<input type="text" size="25" name="loyer_mt" placeholder="loyer mens/trim" required/> 
<input type="hidden" name="asce" value="0"/>   
<input type="checkbox" name="asce" value="1"/> ascenseur    
<input type="hidden" name="hand" value="0"/>   
<input type="checkbox" name="hand" value="1"/> accès handicapé    
<input type="hidden" name="bip" value="0"/>   
<input type="checkbox" name="bip" value="1"/> bip    
<input type="hidden" name="carte_magn" value="0"/>   
<input type="checkbox" name="carte_magn" value="1"/> carte magnétique    </br>
<input type="hidden" name="video_surv" value="0"/>   
<input type="checkbox" name="video_surv" value="1"/> vidéo surveillance    
<input type="hidden" name="cc" value="0"/>   
<input type="checkbox" name="cc" value="1"/> coup de coeur    
<input type="date" name="date_cration" required/> date dossier    
<input type="date" name="date_modif" required/> date modification    
<input type="date" name="date_mand" required/> date mandat    
<input type="date" name="date_fin_mand" required/> date fin mandat    
<input type="date" name="date_liber" required/> date libération    
<input type="date" name="date_disp" required/> date disponibilité    
<input type="date" name="date_fin_bail" /> date fin bail    
<input type="text" size="1024" name="txt_internet" placeholder="texte annonce FR" required/> 
<input type="text" size="25" name="duree_bail" placeholder="durée du bail" required/> 
<input type="text" size="25" name="longeur" placeholder="longeur" required/> 
<input type="text" size="25" name="largeur" placeholder="largeur" required/> 
<input type="text" size="25" name="hauteur" placeholder="hauteur" required/> 
<input type="hidden" name="gardiennage" value="0"/>   
<input type="checkbox" name="gardiennage" value="1"/> gardiennage    
<input type="text" size="25" name="hono_loc_ttc" placeholder="honoraires location TTC" required/> 
<input type="text" size="25" name="tot_hono_loc_ttc" placeholder="total hono.loc.TTC" required/> 
<input type="text" size="100" name="secteur" placeholder="libelle secteur" required/> 
<input type="text" size="512" name="crit_com" placeholder="critères complémentaires" required/> 
<input type="text" size="25" name="ch_mt" placeholder="charges mens/trim" required/> 
<input type="date" name="date_init_ph" required/> date initialise photo    
<input type="text" size="25" name="nom_contact" placeholder="contact annonce" required/> 
<input type="text" size="20" name="tel_contact" placeholder="téléphone contact annonce" required/> 
<input type="text" size="20" name="dist" placeholder="distance en km ville web" required/> 
<input type="text" size="25" name="lat" placeholder="latitude" required/> 
<input type="text" size="25" name="lon" placeholder="longitude" required/> 
<input type="hidden" name="prix_conf" value="0"/>   
<input type="checkbox" name="prix_conf" value="1"/> masquer prix    
<input type="text" size="150" name="mot_cle" placeholder="mots clés" required/> 
<input type="text" size="100" name="contact" placeholder="contacts" required/> 
<input type="text" size="100" name="agence" placeholder="agence" required/> 
<input type="text" size="100" name="langue" placeholder="langues" required/> 
<input type="text" size="25" name="indice_nature_charge" placeholder="indice nature charge" required/> 
<input type="text" size="25" name="complement_loyer" placeholder="complément de loyer" required/> 
<input type="text" size="25" name="indice_complement_loyer" placeholder="indice règlement loyer" required/> 
<input type="text" size="25" name="indice_reglement_charge" placeholder="indice règlement charges" required/> 
<input type="text" size="25" name="hono_etat_lieu_loc" placeholder="honoraires état des lieux" required/> 
</br>
<br/>
<center><input type="submit" value="Valider le formulaire"/></center>
</form>

</ul>
</div>
