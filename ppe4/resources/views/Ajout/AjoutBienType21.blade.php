<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="{{ asset('css/css.css') }}">
</head>
<body>
<div class="accueil">
        <center><h1>Ajout de bien type 21<h1/></center>
@include("../BarreNavigation")
</br>
</div>
<div class="formulairePersonne">
<ul>
<form action="AjoutAnnonceType21" method="post">
{{ csrf_field() }}
<input type="text" size="25" name="noASP" placeholder="numéro ASP" required/> 
<input type="text" size="4" name="code_societe" placeholder="n°société" required/>
<input type="text" size="2" name="code_site" placeholder="numéro site" required/> 
<input type="text" size="5" name="no_dossier" placeholder="numéro dossier" required/> 
<input type="text" size="10" name="no_rue" placeholder="numéro rue" required/> 
<input type="text" size="15" name="type_rue" placeholder="type de rue" required/> 
<input type="text" size="10" name="no_mandat" placeholder="numéro de mandat" required/> 
<input type="text" size="15" name="type_mandat" placeholder="type de mandat" required/> 
<input type="text" size="32" name="adr" placeholder="adresse" required/> 
<input type="text" size="32" name="suite_adr" placeholder="suite adresse" required/> 
<input type="text" size="5" name="cp" placeholder="code postal" required/> 
<input type="text" size="26" name="ville" placeholder="ville" required/> 
<input type="text" size="25" name="pays" placeholder="pays" required/> 
<input type="text" size="25" name="quartier" placeholder="quartier" required/> 
<input type="text" size="25" name="residence" placeholder="residence" required/> 
<input type="text" size="25" name="transport" placeholder="transport" required/> 
<input type="text" size="25" name="proximite" placeholder="proximité" required/> 
<input type="text" size="25" name="situation" placeholder="situation" required/> 
<input type="text" size="40" name="nom_prom" placeholder="nom promoteur" required/> 
<input type="text" size="32" name="nom_prg" placeholder="nom programme" required/> 
<input type="text" size="150" name="url_video" placeholder="url vidéo" required/> 
<input type="text" size="25" name="type1" placeholder="type 1" required/> 
<input type="text" size="25" name="type2" placeholder="type 2" required/> 
<input type="text" size="25" name="type3" placeholder="type 3" required/> 
<input type="text" size="25" name="type4" placeholder="type 4" required/> 
<input type="text" size="25" name="prix_mini" placeholder="prix mini" required/> 
<input type="text" size="25" name="tx_hono" placeholder="taux honoraires" required/> 
<input type="text" size="25" name="prix_max" placeholder="prix max" required/> 
<input type="text" size="25" name="type_bien" placeholder="type bien" required/> 
<input type="text" size="25" name="etat_avanc" placeholder="état avancement" required/> 
<input type="text" size="25" name="pce_min" placeholder="pièce mini" required/> 
<input type="text" size="25" name="pce_max" placeholder="pièce max" required/> 
<input type="text" size="25" name="collind" placeholder="coll / ind" required/> 
<input type="text" size="25" name="nb_type1" placeholder="nb type 1" required/> 
<input type="text" size="25" name="nb_type2" placeholder="nb type 2" required/> 
<input type="text" size="25" name="nb_type3" placeholder="nb type 3" required/> 
<input type="text" size="25" name="nb_type4" placeholder="nb type 4" required/> 
<input type="text" size="25" name="nb_type5" placeholder="nb type 5" required/> 
<input type="text" size="25" name="nb_type6" placeholder="nb type 6" required/> 
<input type="hidden" name="cc" value="0"/>   
<input type="checkbox" name="cc" value="1"/> coup de coeur    </br>
<input type="date" name="date_cration" required/> date dossier    
<input type="date" name="date_modif" required/> date modification    
<input type="date" name="date_mand" required/> date mandat    
<input type="date" name="date_fin_mand" required/> date fin mandat    
<input type="text" size="1024" name="txt_internet" placeholder="texte annonce FR" required/> 
<input type="text" size="25" name="type5" placeholder="type 5" required/> 
<input type="text" size="25" name="type6" placeholder="type 6" required/> 
<input type="text" size="100" name="secteur" placeholder="libelle secteur" required/> 
<input type="text" size="512" name="crit_com" placeholder="critères complémentaires" required/> 
<input type="hidden" name="prix_conf" value="0"/>   
<input type="checkbox" name="prix_conf" value="1"/> masquer prix    
<input type="text" size="150" name="mot_cle" placeholder="mots clés" required/> 
<input type="text" size="100" name="canaux" placeholder="canaux" required/> 
<input type="text" size="100" name="filtres" placeholder="filtres" required/> 
<input type="text" size="100" name="lots_neuf" placeholder="lots neufs" required/> 
<input type="text" size="100" name="contact" placeholder="contacts" required/> 
<input type="text" size="100" name="agence" placeholder="agence" required/> 
<input type="text" size="100" name="langues" placeholder="langues" required/> 
</br>
<br/>
<center><input type="submit" value="Valider le formulaire"/></center>
</form>

</ul>
</div>
