<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="{{ asset('css/css.css') }}">
</head>
<body>
<div class="accueil">
        <center><h1>Ajout de bien type 6<h1/></center>
@include("../BarreNavigation")
</br>
</div>
<div class="formulairePersonne">
<ul>
<form action="AjoutAnnonceType6" method="post">
{{ csrf_field() }}
<input type="text" size="25" name="noASP" placeholder="numéro ASP" required/> 
<input type="text" size="4" name="code_societe" placeholder="n°société" required/>
<input type="text" size="2" name="code_site" placeholder="numéro site" required/> 
<input type="text" size="5" name="no_dossier" placeholder="numéro dossier" required/> 
<input type="text" size="10" name="no_rue" placeholder="numéro rue" required/> 
<input type="text" size="15" name="type_rue" placeholder="type de rue" required/> 
<input type="text" size="10" name="no_mandat" placeholder="numéro de mandat" required/> 
<input type="text" size="15" name="type_mandat" placeholder="type de mandat" required/> 
<input type="text" size="32" name="adr" placeholder="adresse" required/> 
<input type="text" size="32" name="suite_adr" placeholder="suite adresse" required/> 
<input type="text" size="5" name="cp" placeholder="code postal" required/> 
<input type="text" size="26" name="ville" placeholder="ville" required/> 
<input type="text" size="25" name="pays" placeholder="pays" required/> 
<input type="text" size="5" name="cp_web" placeholder="cp internet" required/> 
<input type="text" size="26" name="ville_web" placeholder="ville internet" required/> 
<input type="text" size="25" name="reg_fisc" placeholder="régime fiscal" required/> 
<input type="text" size="25" name="forme_jur" placeholder="forme juridique" required/> 
<input type="text" size="25" name="cat" placeholder="catégorie (sous type de bien)" required/> 
<input type="text" size="25" name="activite" placeholder="activité" required/> 
<input type="text" size="25" name="raison_soc" placeholder="raison sociale" required/> 
<input type="text" size="25" name="enseigne" placeholder="enseigne" required/> 
<input type="text" size="150" name="url_video" placeholder="url visite/video" required/> 
<input type="text" size="25" name="type_bail" placeholder="type bail" required/> 
<input type="text" size="10" name="digicode" placeholder="digicode" required/> 
<input type="text" size="25" name="pv" placeholder="prix vente / prix net acquéreur" required/> 
<input type="text" size="25" name="hono" placeholder="honoraire TTC" required/> 
<input type="text" size="25" name="tx_hono" placeholder="taux honoraires" required/> 
<input type="text" size="25" name="cap_soc" placeholder="capital social" required/> 
<input type="text" size="25" name="taxe_prof" placeholder="taxe professionnelle" required/> 
<input type="text" size="25" name="mas_sal" placeholder="masse salariale" required/> 
<input type="text" size="25" name="charges" placeholder="charges" required/> 
<input type="text" size="25" name="loyer" placeholder="loyer" required/> 
<input type="text" size="25" name="surf_prof" placeholder="surface proffess." required/> 
<input type="text" size="25" name="surf_annexe" placeholder="surface annexe" required/> 
<input type="text" size="25" name="surf_terr" placeholder="surface terrain" required/> 
<input type="text" size="25" name="type_bien" placeholder="type de bien" required/> 
<input type="text" size="25" name="etat_avanc" placeholder="état avancement" required/> 
<input type="text" size="25" name="nb_pce" placeholder="nombre de pièces" required/> 
<input type="text" size="25" name="nb_emp" placeholder="nombre d'employés" required/> 
<input type="text" size="25" name="annee1" placeholder="année 1" required/> 
<input type="text" size="25" name="annee2" placeholder="année 2" required/> 
<input type="text" size="25" name="annee3" placeholder="année 3" required/> 
<input type="text" size="25" name="nb_park_int" placeholder="nombre parking intérieur" required/>
<input type="text" size="25" name="nb_park_ext" placeholder="nombre parking extérieur" required/> 
<input type="text" size="25" name="nb_box" placeholder="nombre boxe" required/>  
<input type="hidden" name="cc" value="0"/>   
<input type="checkbox" name="cc" value="1"/> coup de coeur    
<input type="date" name="date_cration" required/> date dossier    </br>
<input type="date" name="date_modif" required/> date modification    
<input type="date" name="date_mand" required/> date mandat    
<input type="date" name="date_fin_mand" required/> date fin mandat    
<input type="date" name="date_creat_fdc" required/> date création FDC    
<input type="date" name="date_modif_prix" required/> date modification prix    
<input type="text" size="1024" name="txt_internet" placeholder="texte annonce FR" required/> 
<input type="text" size="1024" name="txt_presse" placeholder="texte mailing" required/> 
<input type="text" size="25" name="lon_vit" placeholder="longueur vitrine" required/> 
<input type="text" size="25" name="surf_log" placeholder="surface logement" required/> 
<input type="text" size="25" name="lon_vit" placeholder="longueur vitrine" required/> 
<input type="text" size="25" name="taux_taxe_prof" placeholder="taux taxe pro." required/> 
<input type="text" size="25" name="ca_an1" placeholder="chiffre affaire année 1" required/> 
<input type="text" size="25" name="ca_an2" placeholder="chiffre affaire année 2" required/> 
<input type="text" size="25" name="ca_an3" placeholder="chiffre affaire année 3" required/> 
<input type="text" size="25" name="bic_an1" placeholder="BIC année 1" required/> 
<input type="text" size="25" name="bic_an2" placeholder="BIC année 2" required/> 
<input type="text" size="25" name="bic_an3" placeholder="BIC année 3" required/> 
<input type="text" size="25" name="sal_exploit" placeholder="salaire exploitant" required/> 
<input type="text" size="25" name="val_stock" placeholder="valeur stock" required/> 
<input type="text" size="25" name="cash_flow" placeholder="cash flow" required/> 
<input type="text" size="25" name="non_dpe" placeholder="1=non assujetti DPE, 2= DPE vierge" required/> 
<input type="text" size="10" name="no_priv" placeholder="no_contrat privilège" required/> 
<input type="text" size="100" name="secteur" placeholder="libelle secteur" required/> 
<input type="text" size="512" name="crit_com" placeholder="critères complémentaires" required/> 
<input type="date" name="date_fin_bail" required/> date échéance bail    
<input type="date" name="date_init_ph" required/> date initialise photo    
<input type="text" size="25" name="nom_contact" placeholder="contact annonce" required/> 
<input type="text" size="20" name="tel_contact" placeholder="téléphone contact annonce" required/> 
<input type="text" size="20" name="dist" placeholder="distance en km ville web" required/> 
<input type="text" size="25" name="ch_hono" placeholder="0=vendeur" required/> 
<input type="text" size="1" name="dpe" placeholder="étiquette consomation" required/> 
<input type="text" size="1" name="ges" placeholder="étiquette émission ges" required/> 
<input type="text" size="25" name="val_dpe" placeholder="valeur consomation" required/> 
<input type="text" size="25" name="val_ges" placeholder="valeur émission ges" required/> 
<input type="hidden" name="cpg_laf" value="0"/>   
<input type="checkbox" name="cpg_laf" value="1"/> campagne nationale laforêt    
<input type="text" size="25" name="lat" placeholder="latitude" required/> 
<input type="text" size="25" name="lon" placeholder="longitude" required/> 
<input type="hidden" name="prix_conf" value="0"/>   
<input type="checkbox" name="prix_conf" value="1"/> masquer prix    
<input type="text" size="150" name="mot_cle" placeholder="mots clés" required/> 
<input type="text" size="100" name="contact" placeholder="contacts" required/> 
<input type="text" size="100" name="agence" placeholder="agence" required/> 
<input type="text" size="100" name="langue" placeholder="langues" required/> 
<input type="text" size="25" name="copropriete" placeholder="copropriété 1=oui/2=non" required/> 
<input type="text" size="25" name="nb_lots_copro" placeholder="nombre de lots de la copro." required/> 
<input type="text" size="25" name="montant_quotepart" placeholder="montant quotepart" required/> 
<input type="text" size="25" name="procedure_synd" placeholder="procedure syndicat 1=oui/2=non" required/> 
<input type="text" size="128" name="procedure_detail" placeholder="détail de la procédure syndicat copro." required/> 
</br>
<br/>
<center><input type="submit" value="Valider le formulaire"/></center>
</form>

</ul>
</div>
