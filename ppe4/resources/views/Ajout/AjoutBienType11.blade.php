<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="{{ asset('css/css.css') }}">
</head>
<body>
<div class="accueil">
        <center><h1>Ajout de bien type 11<h1/></center>
@include("../BarreNavigation")
</br>
</div>
<div class="formulairePersonne">
<ul>
<form action="AjoutAnnonceType11" method="post">
{{ csrf_field() }}
<input type="text" size="25" name="noASP" placeholder="numéro ASP" required/> 
<input type="text" size="4" name="code_societe" placeholder="n°société" required/>
<input type="text" size="2" name="code_site" placeholder="numéro site" required/> 
<input type="text" size="5" name="no_dossier" placeholder="numéro dossier" required/> 
<input type="text" size="10" name="no_rue" placeholder="numéro rue" required/> 
<input type="text" size="15" name="type_rue" placeholder="type de rue" required/> 
<input type="text" size="10" name="no_mandat" placeholder="numéro de mandat" required/> 
<input type="text" size="15" name="type_mandat" placeholder="type de mandat" required/> 
<input type="text" size="32" name="adr" placeholder="adresse" required/> 
<input type="text" size="32" name="suite_adr" placeholder="suite adresse" required/> 
<input type="text" size="5" name="cp" placeholder="code postal" required/> 
<input type="text" size="26" name="ville" placeholder="ville" required/> 
<input type="text" size="25" name="pays" placeholder="pays" required/> 
<input type="text" size="25" name="quartier" placeholder="quartier" required/> 
<input type="text" size="25" name="residence" placeholder="residence" required/> 
<input type="text" size="25" name="transport" placeholder="transport" required/> 
<input type="text" size="25" name="proximite" placeholder="proximité" required/> 
<input type="text" size="25" name="situation" placeholder="situation" required/> 
<input type="text" size="5" name="cp_web" placeholder="cp internet" required/> 
<input type="text" size="26" name="ville_web" placeholder="ville internet" required/> 
<input type="text" size="25" name="mode_chauff" placeholder="mode chauffage" required/> 
<input type="text" size="25" name="cat" placeholder="catégorie (sous type de bien)" required/> 
<input type="text" size="25" name="type_chauff" placeholder="type chauffage" required/> 
<input type="text" size="25" name="nat_chauff" placeholder="nature chauffage" required/> 
<input type="text" size="25" name="type_cuis" placeholder="type cuisine" required/> 
<input type="text" size="25" name="standing" placeholder="standing" required/> 
<input type="text" size="25" name="type_cons" placeholder="type construction" required/> 
<input type="text" size="25" name="couv" placeholder="couverture" required/> 
<input type="text" size="25" name="facade" placeholder="façade" required/> 
<input type="text" size="25" name="etat_ext" placeholder="état extérieur" required/> 
<input type="text" size="25" name="eau_chaude" placeholder="eau chaude" required/> 
<input type="text" size="10" name="digicode" placeholder="digicode" required/> 
<input type="text" size="2" name="ce" placeholder="code etage" required/> 
<input type="text" size="150" name="url_video" placeholder="url visite/video" required/> 
<input type="text" size="25" name="type_cons" placeholder="type construction" required/> 
<input type="text" size="25" name="taxe_hab" placeholder="taxe habitation" required/> 
<input type="text" size="25" name="ref_adb" placeholder="référence ADB" required/> 
<input type="text" size="25" name="regime_bail" placeholder="régime bail" required/> 
<input type="text" size="25" name="type_caution" placeholder="type de caution demandée" required/>
<input type="text" size="25" name="loyer_hc" placeholder="loyer H.C" required/>  
<input type="text" size="25" name="loyer_park" placeholder="loyer park./garage" required/> 
<input type="text" size="25" name="prov_ch" placeholder="prov. CH." required/> 
<input type="text" size="25" name="prov_chauff" placeholder="provision chauffage" required/> 
<input type="text" size="25" name="frais_div" placeholder="frais divers" required/> 
<input type="text" size="25" name="loyer_cc" placeholder="loyer C.C." required/> 
<input type="text" size="25" name="travaux" placeholder="travaux" required/> 
<input type="text" size="25" name="dep_g" placeholder="dépôt garantie" required/> 
<input type="text" size="25" name="hral" placeholder="hono.rédact.acte locat." required/> 
<input type="text" size="25" name="surf_hab" placeholder="surface habitable" required/> 
<input type="text" size="25" name="hrap" placeholder="hono.rédact.acte prop." required/> 
<input type="text" size="25" name="surf_jar" placeholder="surface jardin" required/> 
<input type="text" size="25" name="type_bien" placeholder="type de bien" required/> 
<input type="text" size="25" name="etat_avanc" placeholder="état avancement" required/> 
<input type="text" size="25" name="nb_pce" placeholder="nombre de pièces" required/> 
<input type="text" size="25" name="nb_chb" placeholder="nombre de chambres" required/> 
<input type="text" size="25" name="etage" placeholder="étage" required/> 
<input type="text" size="25" name="nb_etage" placeholder="nombres d'étages" required/> 
<input type="text" size="25" name="nb_wc" placeholder="nombre de wc" required/> 
<input type="text" size="25" name="nb_sdb" placeholder="nombre salles de bain" required/> 
<input type="text" size="25" name="nb_se" placeholder="nombre salles d'eau" required/> 
<input type="text" size="25" name="nb_park_int" placeholder="nombre parking intérieur" required/>
<input type="text" size="25" name="nb_park_ext" placeholder="nombre parking extérieur" required/> 
<input type="text" size="25" name="nb_box" placeholder="nombre boxe" required/>  
<input type="text" size="25" name="annee_cons" placeholder="année construction" required/> 
<input type="text" size="25" name="nb_cave" placeholder="nombres caves" required/> 
<input type="text" size="25" name="nb_balcon" placeholder="nombre balcons" required/> 
<input type="hidden" name="asce" value="0"/>   
<input type="checkbox" name="asce" value="1"/> ascenseur    
<input type="hidden" name="hand" value="0"/>   
<input type="checkbox" name="hand" value="1"/> accès handicapé    
<input type="hidden" name="interphone" value="0"/>   
<input type="checkbox" name="interphone" value="1"/> interphone    
<input type="hidden" name="has_digicode" value="0"/>   
<input type="checkbox" name="has_digicode" value="1"/> digicode    
<input type="hidden" name="caution" value="0"/>   
<input type="checkbox" name="caution" value="1"/> caution    
<input type="hidden" name="occ_prop" value="0"/>   
<input type="checkbox" name="occ_prop" value="1"/> occupé propriétaire    
<input type="hidden" name="cc" value="0"/>   
<input type="checkbox" name="cc" value="1"/> coup de coeur    
<input type="hidden" name="piscine" value="0"/>   
<input type="checkbox" name="piscine" value="1"/> piscine    
<input type="hidden" name="meuble" value="0"/>   
<input type="checkbox" name="meuble" value="1"/> meublé    
<input type="date" name="date_cration" required/> date dossier    
<input type="date" name="date_modif" required/> date modification    
<input type="date" name="date_mand" required/> date mandat    
<input type="date" name="date_fin_mand" required/> date fin mandat    
<input type="date" name="date_liber" required/> date libération    
<input type="date" name="date_disp" required/> date disponibilité    </br>
<input type="date" name="date_fin_bail" required/> date fin bail    
<input type="text" size="1024" name="txt_internet" placeholder="texte annonce FR" required/> 
<input type="text" size="25" name="etat_int" placeholder="état intérieur" required/> 
<input type="text" size="25" name="nb_terr" placeholder="nombre terrasses" required/> 
<input type="text" size="25" name="nb_niv" placeholder="nombre niveau" required/> 
<input type="text" size="25" name="duree_bail" placeholder="durée du bail" required/> 
<input type="text" size="25" name="surf_sej" placeholder="surface séjour" required/> 
<input type="text" size="25" name="non_dpe" placeholder="1=non assujetti DPE, 2= DPE vierge" required/> 
<input type="text" size="25" name="hono_loc" placeholder="total honoraires location" required/> 
<input type="text" size="100" name="secteur" placeholder="libelle secteur" required/> 
<input type="text" size="512" name="crit_com" placeholder="critères complémentaires" required/> 
<input type="text" size="25" name="mandat_alg" placeholder="type mandat autre/gestion/location" required/> 
<input type="date" name="date_init_ph" required/> date initialise photo    
<input type="date" name="date_dpe" required/> date diag. D.P.E.    
<input type="date" name="date_ernt" required/> date diag. E.R.N.T.    
<input type="date" name="date_pb" required/> date diag. plomb    
<input type="text" size="25" name="presta_dpe" placeholder="prestataire diag. D.P.E." required/> 
<input type="text" size="25" name="presta_erng" placeholder="prestataire diag. E.R.N.T." required/> 
<input type="text" size="25" name="presta_pb" placeholder="prestataire diag. plomb" required/> 
<input type="text" size="25" name="nom_contact" placeholder="contact annonce" required/> 
<input type="text" size="20" name="tel_contact" placeholder="téléphone contact annonce" required/> 
<input type="text" size="20" name="dist" placeholder="distance en km ville web" required/> 
<input type="text" size="25" name="total_hono_ttc" placeholder="total hono. agence TTC" required/> 
<input type="text" size="1" name="dpe" placeholder="étiquette consomation" required/> 
<input type="text" size="1" name="ges" placeholder="étiquette émission ges" required/> 
<input type="text" size="25" name="val_dpe" placeholder="valeur consomation" required/> 
<input type="text" size="25" name="val_ges" placeholder="valeur émission ges" required/> 
<input type="text" size="25" name="total_hono_ht" placeholder="total hono. agence HT" required/> 
<input type="hidden" name="ali" value="0"/>   
<input type="checkbox" name="ali" value="1"/> Assurance loyer impayés    
<input type="text" size="25" name="lat" placeholder="latitude" required/> 
<input type="text" size="25" name="lon" placeholder="longitude" required/> 
<input type="hidden" name="prix_conf" value="0"/>   
<input type="checkbox" name="prix_conf" value="1"/> masquer prix    
<input type="text" size="150" name="mot_cle" placeholder="mots clés" required/> 
<input type="text" size="100" name="contact" placeholder="contacts" required/> 
<input type="text" size="100" name="agence" placeholder="agence" required/> 
<input type="text" size="100" name="langue" placeholder="langues" required/> 
<input type="text" size="25" name="indice_nature_charge" placeholder="indice nature charge" required/> 
<input type="text" size="25" name="complement_loyer" placeholder="complément de loyer" required/> 
<input type="text" size="25" name="indice_complement_loyer" placeholder="indice règlement loyer" required/> 
<input type="text" size="25" name="indice_reglement_charge" placeholder="indice règlement charges" required/> 
<input type="text" size="25" name="hono_etat_lieu_loc" placeholder="honoraires état des lieux" required/> 
</br>
<br/>
<center><input type="submit" value="Valider le formulaire"/></center>
</form>

</ul>
</div>
