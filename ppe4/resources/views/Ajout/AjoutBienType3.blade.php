<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="{{ asset('css/css.css') }}">
</head>
<body>
<div class="accueil">
        <center><h1>Ajout de bien type 3<h1/></center>
@include("../BarreNavigation")
</br>
</div>
<div class="formulairePersonne">
<ul>
<form action="AjoutAnnonceType3" method="post">
{{ csrf_field() }}
<input type="text" size="25" name="noASP" placeholder="numéro ASP" required/> 
<input type="text" size="4" name="code_societe" placeholder="n°société" required/>
<input type="text" size="2" name="code_site" placeholder="numéro site" required/> 
<input type="text" size="5" name="no_dossier" placeholder="numéro dossier" required/> 
<input type="text" size="10" name="no_rue" placeholder="numéro rue" required/> 
<input type="text" size="15" name="type_rue" placeholder="type de rue" required/> 
<input type="text" size="10" name="no_mandat" placeholder="numéro de mandat" required/> 
<input type="text" size="15" name="type_mandat" placeholder="type de mandat" required/> 
<input type="text" size="32" name="adr" placeholder="adresse" required/> 
<input type="text" size="32" name="suite_adr" placeholder="suite adresse" required/> 
<input type="text" size="5" name="cp" placeholder="code postal" required/> 
<input type="text" size="26" name="ville" placeholder="ville" required/> 
<input type="text" size="25" name="pays" placeholder="pays" required/> 
<input type="text" size="25" name="quartier" placeholder="quartier" required/> 
<input type="text" size="25" name="residence" placeholder="residence" required/> 
<input type="text" size="25" name="transport" placeholder="transport" required/> 
<input type="text" size="25" name="proximite" placeholder="proximité" required/> 
<input type="text" size="25" name="situation" placeholder="situation" required/> 
<input type="text" size="5" name="cp_web" placeholder="cp internet" required/> 
<input type="text" size="26" name="ville_web" placeholder="ville internet" required/> 
<input type="text" size="25" name="mode_chauff" placeholder="mode chauffage" required/> 
<input type="text" size="25" name="reg_fisc" placeholder="régime fiscal" required/> 
<input type="text" size="25" name="forme_jur" placeholder="forme juridique" required/> 
<input type="text" size="25" name="cat" placeholder="catégorie (sous type de bien)" required/> 
<input type="text" size="150" name="url_video" placeholder="url visite/video" required/> 
<input type="text" size="25" name="aspect" placeholder="aspect terrain" required/> 
<input type="text" size="25" name="pv" placeholder="prix vente / prix net acquéreur" required/> 
<input type="text" size="25" name="hono" placeholder="honoraire TTC" required/> 
<input type="text" size="25" name="tx_hono" placeholder="taux honoraires" required/> 
<input type="text" size="25" name="taxe_fonc" placeholder="taxe foncière" required/> 
<input type="text" size="25" name="long_fac" placeholder="longueur façade" required/> 
<input type="text" size="25" name="cos" placeholder="cos" required/> 
<input type="text" size="25" name="type_bien" placeholder="type de bien" required/> 
<input type="text" size="25" name="etat_avanc" placeholder="état avancement" required/> 
<input type="hidden" name="cc" value="0"/>   
<input type="checkbox" name="cc" value="1"/> coup de coeur    
<input type="hidden" name="viab" value="0"/>   
<input type="checkbox" name="viab" value="1"/> viabilité    
<input type="hidden" name="gaz" value="0"/>   
<input type="checkbox" name="gaz" value="1"/> gaz    
<input type="hidden" name="ass" value="0"/>   
<input type="checkbox" name="ass" value="1"/> assainissement    
<input type="hidden" name="align" value="0"/>   
<input type="checkbox" name="align" value="1"/> alignement    
<input type="hidden" name="clot" value="0"/>   
<input type="checkbox" name="clot" value="1"/> cloturé    
<input type="date" name="date_cration" required/> date dossier    
<input type="date" name="date_modif" required/> date modification    
<input type="date" name="date_mand" required/> date mandat    
<input type="date" name="date_fin_mand" required/> date fin mandat    
<input type="date" name="date_certif_urb" required/> date certificat urbain    
<input type="date" name="date_certif_am" required/> date certicicat aménagement    
<input type="text" size="1024" name="txt_internet" placeholder="texte annonce FR" required/> 
<input type="text" size="1024" name="txt_presse" placeholder="texte mailing" required/> 
<input type="text" size="25" name="prof" placeholder="profondeur" required/> 
<input type="text" size="25" name="haut_max" placeholder="hauteur max" required/> 
<input type="text" size="25" name="surf_planch" placeholder="surface de plancher (S.H.O.N.)" required/> 
<input type="text" size="10" name="no_priv" placeholder="no_contrat privilège" required/> 
<input type="text" size="100" name="secteur" placeholder="libelle secteur" required/> 
<input type="text" size="512" name="crit_com" placeholder="critères complémentaires" required/> 
<input type="date" name="date_init_ph" required/> date initialise photo    
<input type="text" size="25" name="nom_contact" placeholder="contact annonce" required/> 
<input type="text" size="20" name="tel_contact" placeholder="téléphone contact annonce" required/> 
<input type="text" size="20" name="dist" placeholder="distance en km ville web" required/> 
<input type="text" size="25" name="ch_hono" placeholder="0=vendeur" required/> 
<input type="hidden" name="cpg_laf" value="0"/>   
<input type="checkbox" name="cpg_laf" value="1"/> campagne nationale laforêt    
<input type="text" size="25" name="lat" placeholder="latitude" required/> 
<input type="text" size="25" name="lon" placeholder="longitude" required/> 
<input type="hidden" name="prix_conf" value="0"/>   
<input type="checkbox" name="prix_conf" value="1"/> masquer prix    
<input type="text" size="150" name="mot_cle" placeholder="mots clés" required/> 
<input type="text" size="100" name="contact" placeholder="contacts" required/> 
<input type="text" size="100" name="agence" placeholder="agence" required/> 
<input type="text" size="100" name="langue" placeholder="langues" required/> 
<input type="text" size="25" name="copropriete" placeholder="copropriété 1=oui/2=non" required/> 
<input type="text" size="25" name="nb_lots_copro" placeholder="nombre de lots de la copro." required/> 
<input type="text" size="25" name="montant_quotepart" placeholder="montant quotepart" required/> 
<input type="text" size="25" name="procedure_synd" placeholder="procedure syndicat 1=oui/2=non" required/> 
<input type="text" size="128" name="procedure_detail" placeholder="détail de la procédure syndicat copro." required/> 
</br>
<br/>
<center><input type="submit" value="Valider le formulaire"/></center>
</form>

</ul>
</div>
