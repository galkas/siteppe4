<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="{{ asset('css/css.css') }}">
</head>
<body>
<div class="accueil">
        <center><h1>Ajout de lot neuf<h1/></center>
@include("../BarreNavigation")
</br>
</div>
<div class="formulairePersonne">
<ul>
<form action="AjoutLotsNeufs" method="post">
{{ csrf_field() }}
<input type="text" size="25" name="noASP" placeholder="numéro ASP" required/> 
<input type="text" size="25" name="etat_avanc" placeholder="état lot" required/> 
<input type="text" size="25" name="type_bien" placeholder="type d'offre" required/> 
<input type="text" size="25" name="cat" placeholder="catégorie" required/> 
<input type="text" size="25" name="no_lot" placeholder="numéro du lot" required/> 
<input type="text" size="25" name="nb_pce" placeholder="nombre de pièces" required/> 
<input type="text" size="25" name="nb_chb" placeholder="nombre de chambres" required/> 
<input type="text" size="25" name="surf" placeholder="surface" required/> 
<input type="text" size="25" name="etage" placeholder="étage" required/> 
<input type="text" size="25" name="nb_etage" placeholder="nb_etage" required/> 
<input type="text" size="25" name="surf_jar" placeholder="surface jardin" required/> 
<input type="text" size="25" name="surf_terr" placeholder="surface terrasse" required/> 
<input type="text" size="25" name="vue" placeholder="vue" required/> 
<input type="text" size="2" name="expo" placeholder="exposition" required/> 
<input type="text" size="25" name="pv" placeholder="prix de vente" required/> 
<input type="date" name="date_liv" required/> date livraison    
<input type="text" size="25" name="nb_park" placeholder="nombre parking" required/> 
<input type="text" size="25" name="type_park" placeholder="type de parking" required/> 
<input type="text" size="25" name="prix_park" placeholder="prix parking" required/> 
<input type="text" size="25" name="loyer_gar" placeholder="loyer garantis" required/> 
<input type="text" size="25" name="rentab" placeholder="rentabilité" required/> 
<input type="text" size="25" name="no_place" placeholder="numéro de place" required/> 
<input type="text" size="200" name="txt_internet" placeholder="descriptif" required/> 
<input type="text" size="25" name="bat" placeholder="batiment" required/> 
</br>
<br/>
<center><input type="submit" value="Valider le formulaire"/></center>
</form>

</ul>
</div>
