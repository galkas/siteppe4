<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="{{ asset('css/css.css') }}">
</head>
<body>
<div class="accueil">
        <center><h1>Ajout de bien type 1<h1/></center>
@include("../BarreNavigation")
</br>
</div>
<div class="formulairePersonne">
<ul>
<form action="AjoutAnnonceType1" method="post">
{{ csrf_field() }}
<input type="text" size="25" name="noASP" placeholder="numéro ASP" required/> 
<input type="text" size="4" name="code_societe" placeholder="n°société" required/>
<input type="text" size="2" name="code_site" placeholder="numéro site" required/> 
<input type="text" size="5" name="no_dossier" placeholder="numéro dossier" required/> 
<input type="text" size="10" name="no_rue" placeholder="numéro rue" required/> 
<input type="text" size="15" name="type_rue" placeholder="type de rue" required/> 
<input type="text" size="10" name="no_mandat" placeholder="numéro de mandat" required/> 
<input type="text" size="15" name="type_mandat" placeholder="type de mandat" required/> 
<input type="text" size="32" name="adr" placeholder="adresse" required/> 
<input type="text" size="32" name="suite_adr" placeholder="suite adresse" required/> 
<input type="text" size="5" name="cp" placeholder="code postal" required/> 
<input type="text" size="26" name="ville" placeholder="ville" required/> 
<input type="text" size="25" name="pays" placeholder="pays" required/> 
<input type="text" size="25" name="quartier" placeholder="quartier" required/> 
<input type="text" size="25" name="residence" placeholder="residence" required/> 
<input type="text" size="25" name="transport" placeholder="transport" required/> 
<input type="text" size="25" name="proximite" placeholder="proximité" required/> 
<input type="text" size="25" name="situation" placeholder="situation" required/> 
<input type="text" size="5" name="cp_web" placeholder="cp internet" required/> 
<input type="text" size="26" name="ville_web" placeholder="ville internet" required/> 
<input type="text" size="25" name="mode_chauff" placeholder="mode chauffage" required/> 
<input type="text" size="25" name="reg_fisc" placeholder="régime fiscal" required/> 
<input type="text" size="25" name="forme_jur" placeholder="forme juridique" required/> 
<input type="text" size="25" name="cat" placeholder="catégorie (sous type de bien)" required/> 
<input type="text" size="25" name="type_chauff" placeholder="type chauffage" required/> 
<input type="text" size="25" name="nat_chauff" placeholder="nature chauffage" required/> 
<input type="text" size="25" name="type_cuis" placeholder="type cuisine" required/> 
<input type="text" size="25" name="standing" placeholder="standing" required/> 
<input type="text" size="25" name="type_cons" placeholder="type construction" required/> 
<input type="text" size="25" name="couv" placeholder="couverture" required/> 
<input type="text" size="25" name="facade" placeholder="façade" required/> 
<input type="text" size="25" name="etat_ext" placeholder="état extérieur" required/> 
<input type="text" size="25" name="eau_chaude" placeholder="eau chaude" required/> 
<input type="text" size="10" name="digicode" placeholder="digicode" required/> 
<input type="text" size="25" name="certif_car" placeholder="certif.carrez" required/> 
<input type="text" size="25" name="certif_am" placeholder="certif.amiante" required/> 
<input type="text" size="25" name="certif_pb" placeholder="certif.plomb" required/> 
<input type="text" size="25" name="certif_ter" placeholder="certif.termites" required/> 
<input type="text" size="2" name="ce" placeholder="code etage" required/> 
<input type="text" size="150" name="url_video" placeholder="url visite/video" required/> 
<input type="text" size="25" name="pv" placeholder="prix vente / prix net acquéreur" required/> 
<input type="text" size="25" name="hono" placeholder="honoraire TTC" required/> 
<input type="text" size="25" name="tx_hono" placeholder="taux honoraires" required/> 
<input type="text" size="25" name="travaux" placeholder="travaux" required/> 
<input type="text" size="25" name="taxe_hab" placeholder="taxe habitation" required/> 
<input type="text" size="25" name="taxe_fonc" placeholder="taxe foncière" required/> 
<input type="text" size="25" name="charges" placeholder="charges" required/> 
<input type="text" size="25" name="loyer" placeholder="loyer" required/> 
<input type="text" size="25" name="surf_hab" placeholder="surface habitable" required/> 
<input type="text" size="25" name="surf_carr" placeholder="surface carrez" required/> 
<input type="text" size="25" name="surf_jar" placeholder="surface jardin" required/> 
<input type="text" size="25" name="type_bien" placeholder="type de bien" required/> 
<input type="text" size="25" name="etat_avanc" placeholder="état avancement" required/> 
<input type="text" size="25" name="nb_pce" placeholder="nombre de pièces" required/> 
<input type="text" size="25" name="nb_chb" placeholder="nombre de chambres" required/> 
<input type="text" size="25" name="etage" placeholder="étage" required/> 
<input type="text" size="25" name="nb_etage" placeholder="nombres d'étages" required/> 
<input type="text" size="25" name="nb_wc" placeholder="nombre de wc" required/> 
<input type="text" size="25" name="nb_sdb" placeholder="nombre salles de bain" required/> 
<input type="text" size="25" name="nb_se" placeholder="nombre salles d'eau" required/> 
<input type="text" size="25" name="nb_park_int" placeholder="nombre parking intérieur" required/>
<input type="text" size="25" name="nb_park_ext" placeholder="nombre parking extérieur" required/> 
<input type="text" size="25" name="nb_box" placeholder="nombre boxe" required/>  
<input type="text" size="25" name="annee_cons" placeholder="année construction" required/> 
<input type="text" size="25" name="dispo" placeholder="disponibilité" required/> 
<input type="text" size="25" name="nb_cave" placeholder="nombres caves" required/> 
<input type="text" size="25" name="nb_balcon" placeholder="nombre balcons" required/> 
<input type="hidden" name="asce" value="0"/>
<input type="checkbox" name="asce" value="1"/> ascenseur    
<input type="hidden" name="hand" value="0"/>
<input type="checkbox" name="hand" value="1"/> accès handicapé    
<input type="hidden" name="interphone" value="0"/>
<input type="checkbox" name="interphone" value="1"/> interphone    
<input type="hidden" name="digicode" value="0"/>
<input type="checkbox" name="digicode" value="1"/> digicode    
<input type="hidden" name="cc" value="0"/>
<input type="checkbox" name="cc" value="1"/> coup de coeur    
<input type="hidden" name="piscine" value="0"/>
<input type="checkbox" name="piscine" value="1"/> piscine    
<input type="hidden" name="meuble" value="0"/>
<input type="checkbox" name="meuble" value="1"/> meublé    
<input type="date" name="date_cration" required/> date dossier    
<input type="date" name="date_modif" required/> date modification    
<input type="date" name="date_mand" required/> date mandat    
<input type="date" name="date_fin_mand" required/> date fin mandat    
<input type="date" name="date_liber" required/> date libération    
<input type="date" name="date_modif_prix" required/> date modification prix    
<input type="date" name="date_car" required/> date carrez    
<input type="date" name="date_am" required/> date amiante    
<input type="date" name="date_pb" required/> date plomb    
<input type="date" name="date_ter" required/> date_termites    
<input type="text" size="1024" name="txt_internet" placeholder="texte annonce FR" required/> 
<input type="text" size="1024" name="txt_presse" placeholder="texte mailing" required/> 
<input type="text" size="25" name="etat_int" placeholder="état intérieur" required/> 
<input type="text" size="25" name="nb_terr" placeholder="nombre terrasses" required/> 
<input type="text" size="25" name="nb_niv" placeholder="nombre niveau" required/> 
<input type="text" size="25" name="nb_chb_serv" placeholder="nombre chambres service" required/> 
<input type="text" size="25" name=" placeholder="cession droit au bail" required/> 
<input type="text" size="25" name="surf_sej" placeholder="surface séjour" required/> 
<input type="text" size="25" name="viager_age1" placeholder="viager age 1" required/> 
<input type="text" size="25" name="bouquet" placeholder="viager bouquet" required/> 
<input type="text" size="25" name="rente" placeholder="viager rente" required/> 
<input type="hidden" name="gardien" value="0"/>
<input type="checkbox" name="gardien" value="1"/> gardien    
<input type="hidden" name="prestige" value="0"/>
<input type="checkbox" name="prestige" value="1"/> prestige    
<input type="hidden" name="viager" value="0"/>
<input type="checkbox" name="viager" value="1"/> viager    
<input type="hidden" name="viager_libre" value="0"/>
<input type="checkbox" name="viager_libre" value="1"/> viager libre    
<input type="hidden" name="install_gaz" value="0"/>
<input type="checkbox" name="install_gaz" value="1"/> instal gaz -15ans    
<input type="text" size="25" name="non_dpe" placeholder="1=non assujetti DPE, 2= DPE vierge" required/> 
<input type="text" size="10" name="info_balcon" placeholder="info balcon" required/> 
<input type="text" size="10" name="info_terr" placeholder="info terrasse" required/> 
<input type="text" size="10" name="no_priv" placeholder="no_contrat privilège" required/> 
<input type="text" size="100" name="secteur" placeholder="libelle secteur" required/> 
<input type="text" size="512" name="crit_com" placeholder="critères complémentaires" required/> 
<input type="text" size="25" name="viager_age2" placeholder="viager age 2" required/> 
<input type="date" name="date_init_ph" required/> date initialise photo    
<input type="date" name="date_dpe" required/> date diag. D.P.E.    
<input type="date" name="date_ernt" required/> date diag. E.R.N.T.    
<input type="date" name="date_gaz" required/> date diag. gaz    
<input type="text" size="25" name="presta_dpe" placeholder="prestataire diag. D.P.E." required/> 
<input type="text" size="25" name="presta_erng" placeholder="prestataire diag. E.R.N.T." required/> 
<input type="text" size="25" name="presta_gaz" placeholder="prestataire diag. gaz" required/> 
<input type="text" size="25" name="nom_contact" placeholder="contact annonce" required/> 
<input type="text" size="20" name="tel_contact" placeholder="téléphone contact annonce" required/> 
<input type="text" size="20" name="dist" placeholder="distance en km ville web" required/> 
<input type="text" size="25" name="ch_hono" placeholder="0=vendeur" required/> 
<input type="text" size="1" name="dpe" placeholder="étiquette consomation" required/> 
<input type="text" size="1" name="ges" placeholder="étiquette émission ges" required/> 
<input type="text" size="25" name="val_dpe" placeholder="valeur consomation" required/> 
<input type="text" size="25" name="val_ges" placeholder="valeur émission ges" required/> 
<input type="hidden" name="cpg_laf" value="0"/>
<input type="checkbox" name="cpg_laf" value="1"/> campagne nationale laforêt    
<input type="hidden" name="install_elec" value="0"/>
<input type="checkbox" name="install_elec" value="1"/> installation électrique +15ans    
<input type="date" name="date_elec" required/> date diag. électrique    
<input type="text" size="25" name="presta_elec" placeholder="prestataire diag. électrique" required/> 
<input type="text" size="25" name="lat" placeholder="latitude" required/> 
<input type="text" size="25" name="lon" placeholder="longitude" required/> 
<input type="date" name="date_dianc" required/> date diag. dianc    
<input type="text" size="25" name="presta_dianc" placeholder="presta_diag.dianc" required/> 
<input type="hidden" name="tae" value="0"/>
<input type="checkbox" name="tae" value="1"/> flag tae    
<input type="hidden" name="prix_conf" value="0"/>
<input type="checkbox" name="prix_conf" value="1"/> masquer prix    
<input type="text" size="150" name="mot_cle" placeholder="mots clés" required/> 
<input type="text" size="100" name="contact" placeholder="contacts" required/> 
<input type="text" size="100" name="langue" placeholder="langues" required/> 
<input type="text" size="100" name="agence" placeholder="agence" required/> 
<input type="text" size="25" name="copropriete" placeholder="copropriété 1=oui/2=non" required/> 
<input type="text" size="25" name="nb_lots_copro" placeholder="nombre de lots de la copro." required/> 
<input type="text" size="25" name="montant_quotepart" placeholder="montant quotepart" required/> 
<input type="text" size="25" name="procedure_synd" placeholder="procedure syndicat 1=oui/2=non" required/> 
<input type="text" size="128" name="procedure_detail" placeholder="détail de la procédure syndicat copro." required/> 
</br>
<br/>
<center><input type="submit" value="Valider le formulaire"/></center>
</form>
</ul>
</div>
